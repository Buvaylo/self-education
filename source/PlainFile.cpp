#include "stdafx.h"
#include "ntapi.h"
#include "PlainFile.h"

namespace FileSystemUtils
{

    CPlainFile::CPlainFile()
        : m_FileHandle(nullptr)
        , m_OpenStatus(STATUS_UNSUCCESSFUL)
    {
    }

    CPlainFile::CPlainFile(const std::wstring& fileName, EOpenMode openMode)
        : m_FileHandle(nullptr)
    {
        m_OpenStatus = Open(fileName, openMode);
    }

    CPlainFile::~CPlainFile()
    {
        Close();
    }

    CFileError CPlainFile::OpenStatus() const
    {
        return m_OpenStatus;
    }

    CFileError CPlainFile::Open(const std::wstring& fileName, EOpenMode openMode)
    {
        std::wstring ntFileName = NT_PREFIX + fileName;
        UNICODE_STRING uFileName;
        OBJECT_ATTRIBUTES fileObjectAttributes;
        g_nt.RtlInitUnicodeString(&uFileName, ntFileName.c_str());
        InitializeObjectAttributes(&fileObjectAttributes, &uFileName, OBJ_CASE_INSENSITIVE, nullptr, nullptr);

        ACCESS_MASK DesiredAccess = 0;
        ULONG CreateDisposition = 0;
        switch (openMode)
        {
        case ERead:
            DesiredAccess = FILE_GENERIC_READ;
            CreateDisposition = FILE_OPEN;
            break;
        case EWrite:
            DesiredAccess = FILE_GENERIC_WRITE;
            CreateDisposition = FILE_OPEN_IF;
            break;
        case EReadWrite:
            DesiredAccess = FILE_GENERIC_READ | FILE_GENERIC_WRITE;
            CreateDisposition = FILE_OPEN_IF;
            break;
        default:
            DesiredAccess = 0;
            CreateDisposition = FILE_OPEN;
            break;
        }

        IO_STATUS_BLOCK iosb;
        NTSTATUS status = g_nt.NtCreateFile(&m_FileHandle,
            DesiredAccess | SYNCHRONIZE,
            &fileObjectAttributes,
            &iosb,
            0,
            FILE_ATTRIBUTE_NORMAL,
            0, // ShareAccess
            CreateDisposition,
            FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_ALERT,
            nullptr,
            0);

        return status;
    }

    CFileError CPlainFile::Read(LONG64 byteOffset, CFileBytesLen nBytes, void *buffer, CFileBytesLen & bytes)
    {
        LARGE_INTEGER liByteOffset;
        liByteOffset.QuadPart = byteOffset;

        IO_STATUS_BLOCK IoStatusBlock = {0};
        const NTSTATUS status = g_nt.NtReadFile(
            m_FileHandle,
            nullptr, // Event
            nullptr, // PIO_APC_ROUTINE ApcRoutine,
            nullptr, // PVOID ApcContext,
            &IoStatusBlock,
            buffer,
            nBytes,
            &liByteOffset,
            nullptr // PULONG Key
            );

        bytes = static_cast<CFileBytesLen>(IoStatusBlock.Information);
        return status;
    }

    CFileError CPlainFile::Write(LONG64 byteOffset, CFileBytesLen nBytes, const void *buffer, CFileBytesLen & bytes)
    {
        LARGE_INTEGER liByteOffset;
        liByteOffset.QuadPart = byteOffset;

        IO_STATUS_BLOCK IoStatusBlock = {0};
        const NTSTATUS status = g_nt.NtWriteFile(
            m_FileHandle,
            nullptr, // Event
            nullptr, // PIO_APC_ROUTINE  ApcRoutine,
            nullptr, // PVOID ApcContext,
            &IoStatusBlock,
            buffer,
            nBytes,
            &liByteOffset,
            nullptr // PULONG Key
            );

        bytes = static_cast<CFileBytesLen>(IoStatusBlock.Information);
        return status;
    }

    CFileError CPlainFile::Close()
    {
        CFileError result = 0;
        if (m_FileHandle != nullptr)
        {
            result = g_nt.NtClose(m_FileHandle);
            m_FileHandle = nullptr;
        }
        return result;
    }

}
