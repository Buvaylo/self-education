#include "stdafx.h"
#include "ntapi.h"
#include "EncryptedFile.h"

namespace FileSystemUtils
{

    CEncryptedFile::CEncryptedFile(const std::wstring& fileName, EOpenMode openMode, int key)
        : m_File(fileName, openMode)
    {
        for (int i=0; i<256; ++i)
        {
            m_enc[i] = static_cast<unsigned char>(i);
        }

        srand(key);
        for (int i=0; i<500; ++i)
        {
            const int a = rand() & 255;
            const int b = rand() & 255;
            std::swap(m_enc[a], m_enc[b]);
        }

        for (int i=0; i<256; ++i)
        {
            m_dec[m_enc[i]] = static_cast<unsigned char>(i);
        }
    }

    CEncryptedFile::~CEncryptedFile()
    {
    }

    CFileError CEncryptedFile::OpenStatus() const
    {
        return m_File.OpenStatus();
    }

    CFileError CEncryptedFile::Open(const std::wstring& fileName, EOpenMode openMode)
    {
        CFileError result = m_File.Open(fileName, openMode);
        return result;
    }

    CFileError CEncryptedFile::Read(LONG64 offset, CFileBytesLen nbytes, void *buffer, CFileBytesLen & bytes)
    {
        CFileError result = m_File.Read(offset, nbytes, buffer, bytes);
        if (!NT_SUCCESS(result))
        {
            return result;
        }

        unsigned char *s = static_cast<unsigned char *>(buffer);
        for (CFileBytesLen i=0; i<bytes; ++i)
        {
            unsigned byte = s[i];
            byte = m_dec[byte];
            byte = (byte - offset - i) & 255;
            s[i] = static_cast<unsigned char>(byte);
        }

        return result;
    }

    CFileError CEncryptedFile::Write(LONG64 offset, CFileBytesLen nbytes, const void *buffer, CFileBytesLen & bytes)
    {
        const unsigned char *s = static_cast<const unsigned char *>(buffer);

        std::vector<unsigned char> copy(nbytes);

        for (CFileBytesLen i=0; i<nbytes; ++i)
        {
            unsigned pos = (s[i] + offset + i) & 255;
            copy[i] = m_enc[pos];
        }

        CFileError result = m_File.Write(offset, nbytes, &copy[0], bytes);
        return result;
    }

    CFileError CEncryptedFile::Close()
    {
        CFileError result = m_File.Close();
        return result;
    }

}
