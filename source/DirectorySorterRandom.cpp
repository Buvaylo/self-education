#include "stdafx.h"
#include "DirectorySorterRandom.h"

namespace FileSystemUtils
{

    void CDirectorySorterRandom::Sort(std::vector<CDirectoryEntry> & dirEntries, const std::wstring & parent)
    {
        std::random_shuffle(dirEntries.begin(), dirEntries.end());
    }

    void CDirectorySorterRandom::Sort(std::vector<CDirectoryEntryPtr> & dirEntries, const std::wstring & parent)
    {
        std::random_shuffle(dirEntries.begin(), dirEntries.end());
    }

}
