#pragma once

#include "DirectoryWorker.h"

namespace FileSystemUtils
{

    class CDirectoryWorkerPrinter : public IDirectoryWorker
    {
    public:
        CDirectoryWorkerPrinter();
        ~CDirectoryWorkerPrinter();

        IDirectoryWorker::EStatus DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
    };

}
