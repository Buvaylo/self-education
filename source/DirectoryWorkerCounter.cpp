#include "stdafx.h"
#include "ntapi.h"
#include "DirectoryWorkerCounter.h"

namespace FileSystemUtils
{

    CDirectoryWorkerCounter::~CDirectoryWorkerCounter()
    {
    }

    CDirectoryWorkerCounter::CDirectoryWorkerCounter()
    {
        // place for traverse root directory statistic
        m_DirectoryStack.emplace_back(CDirectorySummary());
    }

    void CDirectoryWorkerCounter::Print(std::ostream & out)
    {
        CDirectorySummary & curDir = m_DirectoryStack.back();
        std::cout << "FileCount = " << curDir.m_FileCount << std::endl;
        std::cout << "DirCount = " << curDir.m_DirCount << std::endl;
        std::cout << "SummaryFileSize = " << curDir.m_SummaryFileSize << std::endl;
        std::cout << "AllocationSize = " << curDir.m_SummaryAllocationSize << std::endl;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerCounter::DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        CDirectorySummary & curDir = m_DirectoryStack.back();

        curDir.m_SummaryFileSize       += dirEntry.GetFileSize();
        curDir.m_SummaryAllocationSize += dirEntry.GetAllocationSize();
        curDir.m_FileCount             += 1;

        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerCounter::DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerCounter::DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // start statistic gathering for directory
        m_DirectoryStack.emplace_back(CDirectorySummary());
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerCounter::DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        CDirectorySummary curDir = m_DirectoryStack.back();
        m_DirectoryStack.pop_back();

        CDirectorySummary & parentDir = m_DirectoryStack.back();

        parentDir.m_SummaryFileSize       += curDir.m_SummaryFileSize;
        parentDir.m_SummaryAllocationSize += curDir.m_SummaryAllocationSize;
        parentDir.m_FileCount             += curDir.m_FileCount;
        parentDir.m_DirCount              += curDir.m_DirCount + 1; // +1 for this directory itself as directory for parent statistic
        return IDirectoryWorker::ContinueTraversing;
    }

}
