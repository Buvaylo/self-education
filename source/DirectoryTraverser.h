#pragma once

#include "DirectoryEntry.h"
#include "DirectoryWorker.h"
#include "DirectorySorter.h"

namespace FileSystemUtils
{
    void ReadDirectory(const std::wstring & dirName, std::vector<CDirectoryEntry>& Directory);

    class CDirectoryTraverser
    {
    public:
        IDirectoryWorker::EStatus Traverse(const std::wstring & dirName, IDirectoryWorker & dirWorker, IDirectorySorter & sorter);

    private:
        IDirectoryWorker::EStatus Forward(const std::wstring & dirName, const std::vector<CDirectoryEntry>& Directory, IDirectoryWorker & dirWorker, IDirectorySorter & sorter);

    };

}

