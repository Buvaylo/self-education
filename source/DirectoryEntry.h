#pragma once

#include "ntapi.h"

namespace FileSystemUtils
{
    // simple universal directory entry (POD)
    class CDirectoryEntry
    {
    public:
        explicit CDirectoryEntry(const Nt::FILE_FULL_DIR_INFORMATION & dirInfo);

        bool IsDirectory() const;
        const std::wstring & GetName() const;
        ULONGLONG GetFileSize() const;
        ULONGLONG GetAllocationSize() const;

    private:
        ULONGLONG       m_CreationTime;
        ULONGLONG       m_LastAccessTime;
        ULONGLONG       m_LastWriteTime;
        ULONGLONG       m_ChangeTime;
        ULONGLONG       m_EndOfFile;
        ULONGLONG       m_AllocationSize;
        ULONG           m_FileAttributes;
        std::wstring    m_FileName;
    };

}
