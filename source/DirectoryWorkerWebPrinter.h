#pragma once

#include "DirectoryWorkerPrinter.h"

namespace FileSystemUtils
{

    class CDirectoryWorkerWebPrinter : public CDirectoryWorkerPrinter
    {
    public:
        CDirectoryWorkerWebPrinter(SOCKET socket);
        ~CDirectoryWorkerWebPrinter();

        IDirectoryWorker::EStatus DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;

    private:
        SOCKET m_Socket;
    };

    int RunWebServer(const WCHAR *dirName);

}
