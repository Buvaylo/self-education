#pragma once

namespace FileSystemUtils
{

    class CFileHandleGuardWin32
    {
    public:
        CFileHandleGuardWin32(HANDLE aHandle = INVALID_HANDLE_VALUE): m_Handle(aHandle) {}
        ~CFileHandleGuardWin32() 
        {
            if (m_Handle != INVALID_HANDLE_VALUE)
            {
                ::CloseHandle(m_Handle);
            }
        }

        operator HANDLE() { return m_Handle; }

        HANDLE operator = (HANDLE NewHandle) 
        {
            if (m_Handle != NewHandle) 
            {
                if (m_Handle != INVALID_HANDLE_VALUE) 
                {
                    ::CloseHandle(m_Handle);
                }
                m_Handle = NewHandle;
            }
            return m_Handle;
        }

        HANDLE release() 
        {
            HANDLE temp = m_Handle;
            m_Handle = INVALID_HANDLE_VALUE;
            return temp;
        }

        void CloseHandle()
        {
            if (m_Handle != INVALID_HANDLE_VALUE) 
            {
                ::CloseHandle(m_Handle);
                m_Handle = INVALID_HANDLE_VALUE;
            }
        }

    private:
        CFileHandleGuardWin32(const CFileHandleGuardWin32&) = delete;
        CFileHandleGuardWin32 & operator = (const CFileHandleGuardWin32&) = delete;

    private:
        HANDLE    m_Handle;
    };

}
