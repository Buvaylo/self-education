#include "stdafx.h"
#include "ntapi.h"
#include "DirectoryWorkerWebPrinter.h"

namespace FileSystemUtils
{

    CDirectoryWorkerWebPrinter::CDirectoryWorkerWebPrinter(SOCKET socket)
        : m_Socket(socket)
    {
    }

    CDirectoryWorkerWebPrinter::~CDirectoryWorkerWebPrinter()
    {
    }

    IDirectoryWorker::EStatus CDirectoryWorkerWebPrinter::DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any method for directory node output
        char msg[1000];
        int len = _snprintf_s(msg, _countof(msg), _TRUNCATE, "F %9I64u %9I64u %ws\\%ws\n",
            dirEntry.GetFileSize(),
            dirEntry.GetAllocationSize(),
            parent.c_str(),
            dirEntry.GetName().c_str()
            );
        send(m_Socket, msg, len, 0);
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerWebPrinter::DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerWebPrinter::DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any payload for directory visiting BEFORE its content
        char msg[1000];
        int len = _snprintf_s(msg, _countof(msg), _TRUNCATE, "D %9I64u %9I64u %ws\\%ws\n",
            dirEntry.GetFileSize(),
            dirEntry.GetAllocationSize(),
            parent.c_str(),
            dirEntry.GetName().c_str()
            );
        send(m_Socket, msg, len, 0);
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerWebPrinter::DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any payload for directory visiting AFTER its content
        return IDirectoryWorker::ContinueTraversing;
    }

}
