#pragma once

#include "FileInterface.h"

namespace FileSystemUtils
{

    class CPlainFile : public CFileInterface
    {
    public:
        ~CPlainFile();
        CPlainFile();
        CPlainFile(const std::wstring& fileName, EOpenMode openMode);
        CFileError OpenStatus() const override;
        CFileError Open(const std::wstring& fileName, EOpenMode openMode) override;
        CFileError Read(LONG64 offset, CFileBytesLen nbytes, void *buffer, CFileBytesLen & bytes) override;
        CFileError Write(LONG64 offset, CFileBytesLen nbytes, const void *buffer, CFileBytesLen & bytes) override;
        CFileError Close() override;

    private:
        HANDLE    m_FileHandle;
        LONG      m_OpenStatus;
    };

}
