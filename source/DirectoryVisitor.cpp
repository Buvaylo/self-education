#include "stdafx.h"
#include "DirectoryTraverser.h"
#include "DirectoryVisitorInterface.h"
#include "DirectoryVisitorIterator.h"
#include "DirectorySorterOrdered.h"

// visitor testing example

namespace FileSystemUtils
{
    class CPrinterVisitor : public CDirectoryVisitorInterface
    {
    public:
        void visit(CDirectory& ref, const std::wstring & parent);
        void visit(CFile& ref, const std::wstring & parent);
    };

    void CPrinterVisitor::visit(CDirectory& ref, const std::wstring & parent)
    {
        // implement here any payload for directory visiting
        wprintf( L"D %9I64u %9I64u %ws\\%ws\n",
            ref.GetFileSize(),
            ref.GetAllocationSize(),
            parent.c_str(),
            ref.GetName().c_str()
           );
    }

    void CPrinterVisitor::visit(CFile& ref, const std::wstring & parent)
    {
        // implement here any method for file visiting
        wprintf( L"F %9I64u %9I64u %ws\\%ws\n",
            ref.GetFileSize(),
            ref.GetAllocationSize(),
            parent.c_str(),
            ref.GetName().c_str()
            );
    }

    void RunDirectoryVisitorIterator(const std::wstring & dirName)
    {
        const bool SortContent = true;
        const bool SortAscendant = true;
        const bool GroupDirectories = true;
        const bool DirectoriesFirst = true;
        FileSystemUtils::CDirectorySorterOrdered sorter(SortContent, SortAscendant, GroupDirectories, DirectoriesFirst);
        FileSystemUtils::CPrinterVisitor visitor;

        for (FileSystemUtils::CDirectoryVisitorIterator it(dirName, sorter); it; ++it)
        {
            it->accept(visitor, it.GetPath());
        }
    }
}
