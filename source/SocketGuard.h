#pragma once

class CSocketGuard
{
public:
    void reset(SOCKET socket)
    {
        if (m_Socket)
        {            
            shutdown(m_Socket, SD_BOTH);
            closesocket(m_Socket);
        }
        m_Socket = socket;
    }

    CSocketGuard(SOCKET socket = NULL)
        : m_Socket(NULL)
    {
        reset(socket);
    }

    ~CSocketGuard()
    {
        reset(0);
    }

    operator SOCKET () const { return m_Socket; }

    SOCKET get()
    {
        return m_Socket;
    }

    SOCKET release()
    {
        SOCKET socket = m_Socket;
        m_Socket = NULL;
        return socket;
    }

private:
    CSocketGuard(const CSocketGuard&);
    CSocketGuard& operator = (const CSocketGuard&);

private:
    SOCKET m_Socket;
};
