#pragma once

class CRefCountedBase 
{
public:
    CRefCountedBase(): m_RefCount(0) {}
    virtual ~CRefCountedBase() {}
    template<class T> friend class intrusive_ptr;

private:
    void AddRef()
    {
        InterlockedIncrement(&m_RefCount);
    }
    void Release()
    {
        if (InterlockedDecrement(&m_RefCount) == 0)
        {
            delete this;
        }
    }

private:
    LONG  m_RefCount;
};


template<class T> class intrusive_ptr
{
private:
    typedef intrusive_ptr this_type;

public:
    typedef T element_type;

    intrusive_ptr() : m_Pointer(nullptr)
    {
    }

    intrusive_ptr(T * p)
        : m_Pointer(p)
    {
        if (m_Pointer != nullptr)
            m_Pointer->AddRef();
    }

    template<class U> intrusive_ptr(intrusive_ptr<U> const & rhs)
        : m_Pointer(rhs.get())
    {
        if (m_Pointer != nullptr)
            m_Pointer->AddRef();
    }

    intrusive_ptr(intrusive_ptr const & rhs)
        : m_Pointer(rhs.get())
    {
        if (m_Pointer != nullptr)
            m_Pointer->AddRef();
    }

    ~intrusive_ptr()
    {
        if (m_Pointer != nullptr)
            m_Pointer->Release();
    }

    template<class U> intrusive_ptr & operator = (intrusive_ptr<U> const & rhs)
    {
        this_type(rhs).swap(*this);
        return *this;
    }

    intrusive_ptr & operator = (intrusive_ptr const & rhs)
    {
        this_type(rhs).swap(*this);
        return *this;
    }

    intrusive_ptr & operator=(T * rhs)
    {
        this_type(rhs).swap(*this);
        return *this;
    }

    T * get() const
    {
        return m_Pointer;
    }

    T & operator*() const
    {
        return *m_Pointer;
    }

    T * operator->() const
    {
        return m_Pointer;
    }

    operator bool () const
    {
        return m_Pointer != nullptr;
    }

    void swap(intrusive_ptr & rhs)
    {
        T * tmp = m_Pointer;
        m_Pointer = rhs.m_Pointer;
        rhs.m_Pointer = tmp;
    }

private:
    T * m_Pointer;
};

template<class T, class U> inline bool operator==(intrusive_ptr<T> const & a, intrusive_ptr<U> const & b)
{
    return a.get() == b.get();
}

template<class T, class U> inline bool operator!=(intrusive_ptr<T> const & a, intrusive_ptr<U> const & b)
{
    return a.get() != b.get();
}

template<class T> inline bool operator==(intrusive_ptr<T> const & a, T * b)
{
    return a.get() == b;
}

template<class T> inline bool operator!=(intrusive_ptr<T> const & a, T * b)
{
    return a.get() != b;
}

template<class T> inline bool operator==(T * a, intrusive_ptr<T> const & b)
{
    return a == b.get();
}

template<class T> inline bool operator!=(T * a, intrusive_ptr<T> const & b)
{
    return a != b.get();
}

template<class T> inline bool operator<(intrusive_ptr<T> const & a, intrusive_ptr<T> const & b)
{
    return std::less<T *>()(a.get(), b.get());
}

template<class T> void swap(intrusive_ptr<T> & lhs, intrusive_ptr<T> & rhs)
{
    lhs.swap(rhs);
}

// mem_fn support

template<class T> T * get_pointer(intrusive_ptr<T> const & p)
{
    return p.get();
}

template<class T, class U> intrusive_ptr<T> static_pointer_cast(intrusive_ptr<U> const & p)
{
    return static_cast<T *>(p.get());
}

template<class T, class U> intrusive_ptr<T> const_pointer_cast(intrusive_ptr<U> const & p)
{
    return const_cast<T *>(p.get());
}

template<class T, class U> intrusive_ptr<T> dynamic_pointer_cast(intrusive_ptr<U> const & p)
{
    return dynamic_cast<T *>(p.get());
}

