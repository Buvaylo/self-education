#include "stdafx.h"
#include "ntapi.h"
#include "DirectoryTraverser.h"
#include "DirectorySorterOrdered.h"
#include "DirectoryWorkerWebPrinter.h"
#include "ThreadPool.h"
#include "SocketGuard.h"

#pragma comment(lib, "ws2_32.lib")

namespace
{
    void logprintf(const char *fmt, ...)
    {
        va_list args;

        va_start(args, fmt);
        vprintf(fmt, args);
        fflush(stdout);
        va_end(args);
    }

    void errexit(const char *fmt, ...)
    {
        va_list    args;

        va_start(args, fmt);
        vprintf(fmt, args);
        fflush(stdout);
        va_end(args);
        exit(666);
    }

    SOCKET ServerStart(USHORT port)
    {
        WSADATA WSAData;
        const int rc = WSAStartup(MAKEWORD(2,0), &WSAData);
        if (rc != 0) {
            errexit("WSAStartup Error");
        }

        SOCKET listen_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (listen_socket == INVALID_SOCKET) {
            errexit("Socket Error");
        }

        SOCKADDR_IN    sin;
        sin.sin_family      = AF_INET;
        sin.sin_addr.s_addr = INADDR_ANY;
        sin.sin_port        = htons(port);

        bind(listen_socket, reinterpret_cast<LPSOCKADDR>(&sin), sizeof(SOCKADDR_IN));
        listen(listen_socket, SOMAXCONN);

        logprintf("Server started: %s, status=%s, maxnum=%d maxudp=%d version=%d highversion=%d\n",
            WSAData.szDescription,
            WSAData.szSystemStatus,
            WSAData.iMaxSockets,
            WSAData.iMaxUdpDg,
            WSAData.wVersion,
            WSAData.wHighVersion);

        return (listen_socket);
    }

    class CSocketResponseHelper : public utils::CThreadPoolTaskInterface
    {
    public:
        CSocketResponseHelper(const std::wstring & dirName) : m_dirName(dirName) {}
        ~CSocketResponseHelper() {}
        void DoWork(LPVOID lpThreadParameter);

    private:
        std::wstring GetDirName(const char buf[], size_t len) const;
        std::wstring m_dirName;
    };

    std::wstring CSocketResponseHelper::GetDirName(const char buf[], size_t len) const
    {
        std::string source(&buf[0], &buf[len]);
        std::istringstream in(source);
        std::string str;
        while (std::getline(in, str))
        {
            std::cerr << str << std::endl;
        }

        return std::wstring(str.begin(), str.end());
    }

    void CSocketResponseHelper::DoWork(LPVOID lpJobParameter)
    {
        CSocketGuard serverSocket(reinterpret_cast<SOCKET>(lpJobParameter));
        char buff[16 * 1024];
        const int len = recv(serverSocket, buff, sizeof(buff), 0);

        std::wstring xxx = GetDirName(buff, len);

        static const char text_resp[] =
            "HTTP/1.1 200 Accept\r\n"
            "Content-Type: text/html\r\n"
            "Connection: close\r\n"
            "\r\n"
            "<html><head>\r\n"
            "<title>Web Application Server answer</title>\r\n"
            "</head><body>\r\n"
            "<h1>Directory Web Server</h1>\r\n"
            "<p>List of content:</p>\r\n"
            "<pre>\r\n";

        send(serverSocket, text_resp, sizeof(text_resp)-1, 0);

        const bool SortContent = true;
        const bool SortAscendant = true;
        const bool GroupDirectories = true;
        const bool DirectoriesFirst = true;
        FileSystemUtils::CDirectorySorterOrdered sorter(SortContent, SortAscendant, GroupDirectories, DirectoriesFirst);

        FileSystemUtils::CDirectoryWorkerWebPrinter dirPrinter(serverSocket);
        FileSystemUtils::CDirectoryTraverser DirectoryTraverser;
        DirectoryTraverser.Traverse(m_dirName, dirPrinter, sorter);

        static const char final[] =
            "</pre>\r\n"
            "</body></html>\r\n";

        send(serverSocket, final, sizeof(final)-1, 0);
    }

}

namespace FileSystemUtils
{
    int RunWebServer(const WCHAR *dirName)
    {
        utils::CThreadPool::GetInstance().InitializeThreadPool(4);
        CSocketResponseHelper socketResponseHelper(dirName);

        {
            const USHORT port = 8000;
            CSocketGuard listenSocket(ServerStart(port));

            for (;;)
            {
                SOCKADDR peer;
                int peerlen = sizeof(peer);

                SOCKADDR_IN temp_addr;
                int accepted_len = sizeof(temp_addr);
                SOCKET serverSocket = accept(listenSocket, reinterpret_cast<struct sockaddr *>(&temp_addr), &accepted_len);
                if (serverSocket == INVALID_SOCKET)
                    break;

                if (0 == getpeername(serverSocket, &peer, &peerlen))
                {
                    logprintf("Connect from %s:%d\n", inet_ntoa(reinterpret_cast<SOCKADDR_IN *>(&peer)->sin_addr), ntohs(reinterpret_cast<SOCKADDR_IN *>(&peer)->sin_port));
                }

                utils::CThreadPool::GetInstance().EnQueue(&socketResponseHelper, reinterpret_cast<LPVOID>(serverSocket));
            }
        }
        WSACleanup();
        logprintf("Server stopped\n");
        return 0;
    }

}
