#pragma once

namespace utils
{
    // class for common context for jobs group
    // individual job information should be processed via 'param'
    // this model allow optimization - doesn't allocate individual job contexts (context directly in param)
    class CThreadPoolTaskInterface
    {
    public:
        virtual void DoWork(LPVOID param) = 0;
    };

    // ThreadPool itself is CThreadPoolTaskInterface,
    // it uses common mechanism for sending service tasks (stop etc) to working threads,
    // but doesn't use DoWork
    class CThreadPool : private CThreadPoolTaskInterface
    {
    public:
        ~CThreadPool();

        // Task interface
        void DoWork(LPVOID param) {} // formal implementation, never used

        // Initializes the thread pool, to accept jobs 
        bool InitializeThreadPool(int maxThreadNum);
        // adds a new job item to the thread pool's job queue
        bool EnQueue(CThreadPoolTaskInterface* pThis, LPVOID param);

        // Stops all the worker threads, resets the thread pool, lost any non processed job
        bool StopThreadPool();

        int GetThreadNum() const;
        static CThreadPool & GetInstance();

    private:
        CThreadPool();
        CThreadPool(const CThreadPool&);
        const CThreadPool& operator = (const CThreadPool&);

        // This is the actual thread function, executed by the worker threads
        static DWORD WINAPI WorkerThreadProcedure(LPVOID lpThreadParam);
        bool SendCommand(CThreadPoolTaskInterface* pThis, LPVOID param);
        bool SendCommand(ULONG_PTR code);
        void AddThread();

    private:
        // thread pool should be singleton for better performance
        static CThreadPool     s_ThreadPoolInstance;

        std::vector<HANDLE>    m_WorkingThreads;
        HANDLE                 m_IoCompletionPortHandle;
        volatile bool          m_ExitFlag;
        volatile LONG          m_TaskCounter;
    };

}
