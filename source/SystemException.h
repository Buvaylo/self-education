#pragma once

    class CSystemException : public std::exception
    {
        public:
            CSystemException(NTSTATUS status) : m_String("nt status error"), m_ErrorCode(status) {}
            CSystemException(DWORD errorCode) : m_String("win32 error"), m_ErrorCode(errorCode) {}
            CSystemException(const char *msg) : m_String(msg), m_ErrorCode(GetLastError()) {}
            CSystemException(const char *msg, NTSTATUS status) : m_String(msg), m_ErrorCode(status) {}
            ~CSystemException() {};

            const char* what() const { return m_String.c_str(); };
            LONG GetErrorCode() const { return m_ErrorCode; }

        private:
            std::string  m_String;
            LONG         m_ErrorCode;
    };
