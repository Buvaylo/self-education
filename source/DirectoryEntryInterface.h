#pragma once

#include "intrusive_ptr.h"
#include "DirectoryEntry.h"

namespace FileSystemUtils
{
    class CDirectoryVisitorInterface;

    // abstract directory entry for visitor scheme
    class CDirectoryEntryInterface : public CRefCountedBase, public CDirectoryEntry
    {
    public:
        virtual ~CDirectoryEntryInterface() {}
        virtual void accept(CDirectoryVisitorInterface& v, const std::wstring & parent) = 0;
        explicit CDirectoryEntryInterface(const Nt::FILE_FULL_DIR_INFORMATION & dirInfo) : CDirectoryEntry(dirInfo) {}
    };

    typedef intrusive_ptr<CDirectoryEntryInterface> CDirectoryEntryPtr;

}
