#include "stdafx.h"
#include "ntapi.h"
#include "SystemException.h"
#include "FileSystemUtils.h"
#include "ThreadPool.h"
#include "FileHandleGuardWin32.h"

namespace
{
    const ULONG BlockSize = 1024 * 1024;

    class CFileCopyHelper : public utils::CThreadPoolTaskInterface
    {
    public:
        CFileCopyHelper();
        ~CFileCopyHelper();

        void Copy(const std::wstring & srcFileName, const std::wstring & dstFileName);

    private:
        void DoWork(LPVOID lpThreadParameter);
        static FileSystemUtils::CFilePos GetOffsetFromBlockNumber(ULONG_PTR blockNumber)
        {
            FileSystemUtils::CFilePos result = blockNumber;
            result *= BlockSize;
            return result;
        }

        CFileCopyHelper(CFileCopyHelper&) = delete;
        CFileCopyHelper& operator=(CFileCopyHelper&) = delete;

    private:
        FileSystemUtils::CFileHandleGuardWin32       m_SrcFileHandle;
        FileSystemUtils::CFileHandleGuardWin32       m_DstFileHandle;
        FileSystemUtils::CFilePos                    m_FileSize;
        FileSystemUtils::CFilePos                    m_WorkSize;
        HANDLE                                       m_WaitEvent;
        ULONG                                        m_ErrorStatus;
    };

    CFileCopyHelper::CFileCopyHelper()
        : m_ErrorStatus(0)
        , m_WaitEvent(nullptr)
    {
        m_WaitEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
    }

    CFileCopyHelper::~CFileCopyHelper()
    {
        if (m_WaitEvent != nullptr)
            CloseHandle(m_WaitEvent);
    }

    void CFileCopyHelper::Copy(const std::wstring & srcFileName, const std::wstring & dstFileName)
    {
        m_SrcFileHandle = ::CreateFileW(srcFileName.c_str()
            , GENERIC_READ
            , FILE_SHARE_READ | FILE_SHARE_WRITE
            , nullptr
            , OPEN_EXISTING
            , FILE_ATTRIBUTE_NORMAL
            , nullptr );

        if (m_SrcFileHandle == INVALID_HANDLE_VALUE)
        {
            throw CSystemException(__FUNCTION__ ": src file open failed");
        }

        LARGE_INTEGER liSize = {0};
        if (!GetFileSizeEx(m_SrcFileHandle, &liSize))
        {
            throw CSystemException(__FUNCTION__ ": src file size failed");
        }

        m_FileSize = liSize.QuadPart;

        m_DstFileHandle = ::CreateFileW(dstFileName.c_str()
            , GENERIC_WRITE
            , FILE_SHARE_READ
            , nullptr
            , CREATE_ALWAYS
            , FILE_ATTRIBUTE_NORMAL
            , nullptr );

        if (m_DstFileHandle == INVALID_HANDLE_VALUE)
        {
            throw CSystemException(__FUNCTION__ ": dst file open failed");
        }

        utils::CThreadPool & threadPool = utils::CThreadPool::GetInstance();

        double estTime = Nt::GetSecs();
        m_WorkSize = 0;
        m_ErrorStatus = 0;
        ResetEvent(m_WaitEvent);

        // constrain: for 32bit system maximum filesize for copying is 4 petabytes
        for (ULONG_PTR blockNumber=0; GetOffsetFromBlockNumber(blockNumber) < m_FileSize; ++blockNumber)
        {
            threadPool.EnQueue(this, reinterpret_cast<LPVOID>(blockNumber));
        }

        WaitForSingleObject(m_WaitEvent, INFINITE);

        estTime = Nt::GetSecs() - estTime;
        printf("numThreads=%d Runtime %.4f Secs %I64d MB\n", threadPool.GetThreadNum(), estTime, m_FileSize / 1024 / 1024);

        if (m_ErrorStatus)
            throw CSystemException("some worker runtime error", m_ErrorStatus);
    }

    void CFileCopyHelper::DoWork(LPVOID param)
    {
        if (m_ErrorStatus)
        {
            return;
        }

        const auto blockNumber = reinterpret_cast<ULONG_PTR>(param);
        const auto curFilePosition = GetOffsetFromBlockNumber(blockNumber);
        const auto endPosition = min(curFilePosition + BlockSize, m_FileSize);
        const auto workSize = static_cast<ULONG>(min(endPosition - curFilePosition, BlockSize));

        if (curFilePosition >= m_FileSize)
            return;

        std::vector<char> buffer(BlockSize);

        LARGE_INTEGER offset;
        offset.QuadPart = curFilePosition;

        IO_STATUS_BLOCK IoStatusBlock;
        NTSTATUS status = g_nt.NtReadFile(m_SrcFileHandle,
            nullptr, // Event
            nullptr, // PIO_APC_ROUTINE ApcRoutine,
            nullptr, // PVOID ApcContext,
            &IoStatusBlock,
            &buffer[0],
            workSize,
            &offset,
            nullptr // PULONG Key
            );

        if (!NT_SUCCESS(status))
        {
            printf("ERROR %X on reading file at pos %I64d!\n", status, curFilePosition);
            m_ErrorStatus = status;
            SetEvent(m_WaitEvent);
            return;
        }

        status = g_nt.NtWriteFile(m_DstFileHandle,
            nullptr, // Event
            nullptr, // PIO_APC_ROUTINE  ApcRoutine,
            nullptr, // PVOID ApcContext,
            &IoStatusBlock,
            &buffer[0],
            workSize,
            &offset,
            nullptr // PULONG Key
            );

        if (!NT_SUCCESS(status))
        {
            printf("ERROR %X on writing file at pos %I64d!\n", status, curFilePosition);
            m_ErrorStatus = status;
            SetEvent(m_WaitEvent);
            return;
        }

        const FileSystemUtils::CFilePos newWorkSize = workSize + InterlockedExchangeAdd64(&m_WorkSize, workSize);
        if (newWorkSize >= m_FileSize)
        {
            // full copying is complete
            SetEvent(m_WaitEvent);
        }
    }
}

namespace FileSystemUtils
{
    void CopyFile(const std::wstring & srcFileName, const std::wstring & dstFileName)
    {
        CFileCopyHelper FileCopyHelper;
        FileCopyHelper.Copy(srcFileName, dstFileName);
    }

    void CreateRandomFile(const std::wstring & fileName, CFilePos fileSize)
    {
        printf("create random file %I64d\n", fileSize);

        CFileHandleGuardWin32 dstFileHandle = ::CreateFileW(fileName.c_str()
            , GENERIC_WRITE
            , FILE_SHARE_READ
            , nullptr
            , CREATE_ALWAYS
            , FILE_ATTRIBUTE_NORMAL
            , nullptr );

        if (dstFileHandle == INVALID_HANDLE_VALUE)
        {
            throw CSystemException(__FUNCTION__ ": random file create failed");
        }

        std::vector<char> srcBuffer(BlockSize);
        for (ULONG i=0; i<BlockSize; ++i)
        {
            srcBuffer[i] = rand() & 255;
        }

        for (CFilePos curFilePosition=0; curFilePosition<fileSize; curFilePosition += BlockSize)
        {
            const CFilePos endPosition = min(curFilePosition + BlockSize, fileSize);
            const auto workSize = static_cast<ULONG>(min(endPosition - curFilePosition, BlockSize));

            // make every block individual
            for (ULONG i=0; i<workSize; ++i)
            {
                srcBuffer[i] ^= ((curFilePosition>>20) + i);
            }

            LARGE_INTEGER offset;
            offset.QuadPart = curFilePosition;

            IO_STATUS_BLOCK IoStatusBlock;
            NTSTATUS status = g_nt.NtWriteFile(dstFileHandle,
                nullptr, // Event
                nullptr, // PIO_APC_ROUTINE  ApcRoutine,
                nullptr, // PVOID ApcContext,
                &IoStatusBlock,
                &srcBuffer[0],
                workSize,
                &offset,
                nullptr // PULONG Key
                );

            if (!NT_SUCCESS(status))
            {
                printf("ERROR %X on writing file at pos %I64d!\n", status, curFilePosition);
                throw CSystemException(status);
            }

        }

        FlushFileBuffers(dstFileHandle);
    }

    void CompareFiles(const std::wstring & srcFileName, const std::wstring & dstFileName)
    {
        CFileHandleGuardWin32 srcFileHandle = ::CreateFileW(srcFileName.c_str()
            , GENERIC_READ
            , FILE_SHARE_READ | FILE_SHARE_WRITE
            , nullptr
            , OPEN_EXISTING
            , FILE_ATTRIBUTE_NORMAL
            , nullptr );

        if (srcFileHandle == INVALID_HANDLE_VALUE)
        {
            throw CSystemException(__FUNCTION__ ": src file open failed");
        }

        LARGE_INTEGER srcSize = {0};
        if (!GetFileSizeEx(srcFileHandle, &srcSize))
        {
            throw CSystemException(__FUNCTION__ ": src file size failed");
        }

        CFileHandleGuardWin32 dstFileHandle = ::CreateFileW(dstFileName.c_str()
            , GENERIC_READ
            , FILE_SHARE_READ | FILE_SHARE_WRITE
            , nullptr
            , OPEN_EXISTING
            , FILE_ATTRIBUTE_NORMAL
            , nullptr );

        if (dstFileHandle == INVALID_HANDLE_VALUE)
        {
            throw CSystemException(__FUNCTION__ ": dst file open failed");
        }

        LARGE_INTEGER dstSize = {0};
        if (!GetFileSizeEx(dstFileHandle, &dstSize))
        {
            throw CSystemException(__FUNCTION__ ": dst file size failed");
        }

        if (dstSize.QuadPart != srcSize.QuadPart)
        {
            throw CSystemException(__FUNCTION__ ": src file size != dst file size", 0);
        }

        const CFilePos fileSize = srcSize.QuadPart;

        std::vector<char> srcBuffer(BlockSize);
        std::vector<char> dstBuffer(BlockSize);

        for (CFilePos curFilePosition=0; curFilePosition<fileSize; curFilePosition += BlockSize)
        {
            const CFilePos endPosition = min(curFilePosition + BlockSize, fileSize);
            const auto workSize = static_cast<ULONG>(min(endPosition - curFilePosition, BlockSize));

            LARGE_INTEGER offset;
            offset.QuadPart = curFilePosition;

            IO_STATUS_BLOCK IoStatusBlock;
            NTSTATUS status = g_nt.NtReadFile(srcFileHandle,
                nullptr, // Event
                nullptr, // PIO_APC_ROUTINE ApcRoutine,
                nullptr, // PVOID ApcContext,
                &IoStatusBlock,
                &srcBuffer[0],
                workSize,
                &offset,
                nullptr // PULONG Key
                );

            if (!NT_SUCCESS(status))
            {
                printf("ERROR %X on reading file at position %I64d!\n", status, curFilePosition);
                throw CSystemException(status);
            }

            status = g_nt.NtReadFile(dstFileHandle,
                nullptr, // Event
                nullptr, // PIO_APC_ROUTINE  ApcRoutine,
                nullptr, // PVOID ApcContext,
                &IoStatusBlock,
                &dstBuffer[0],
                workSize,
                &offset,
                nullptr // PULONG Key
                );

            if (!NT_SUCCESS(status))
            {
                printf("ERROR %X on reading file at position %I64d!\n", status, curFilePosition);
                throw CSystemException(status);
            }

            for (ULONG i=0; i<workSize; ++i)
            {
                if (srcBuffer[i] != dstBuffer[i])
                {
                    printf("ERROR on compare data at position %I64d!\n", curFilePosition+i);
                    throw CSystemException(STATUS_UNSUCCESSFUL);
                }
            }
        }

        printf("files are identical\n");
    }

    void TestCopyFile(const std::wstring & srcFileName, const std::wstring & dstFileName, CFilePos fileSize, int numThreadsMax)
    {
        const int ThreadNumThreshold = 64;
        numThreadsMax = min(numThreadsMax, ThreadNumThreshold);
        numThreadsMax = max(numThreadsMax, 1);

        CreateRandomFile(srcFileName, fileSize);

        for (int numThreads=1; numThreads<=numThreadsMax; ++numThreads)
        {
            utils::CThreadPool::GetInstance().InitializeThreadPool(numThreads);
            CopyFile(srcFileName, dstFileName);
            CompareFiles(srcFileName, dstFileName);
        }
    }

}
