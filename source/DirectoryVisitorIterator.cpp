#include "stdafx.h"
#include "DirectoryEntryInterface.h"
#include "DirectoryVisitorInterface.h"
#include "DirectoryVisitorIterator.h"
#include "DirectorySorter.h"
#include "SystemException.h"

namespace FileSystemUtils
{
    void ReadDirectory(const std::wstring & dirName, std::vector<CDirectoryEntryPtr> & Directory)
    {
        std::wstring ntDirName = NT_PREFIX + dirName;
        UNICODE_STRING _DirectoryName;
        OBJECT_ATTRIBUTES dirObjectAttributes;
        g_nt.RtlInitUnicodeString(&_DirectoryName, ntDirName.c_str());
        InitializeObjectAttributes(&dirObjectAttributes, &_DirectoryName, OBJ_CASE_INSENSITIVE, nullptr, nullptr);

        Nt::CFileHandle hFile;
        IO_STATUS_BLOCK iosb;
        NTSTATUS status = g_nt.NtCreateFile(&hFile,
            SYNCHRONIZE | FILE_LIST_DIRECTORY,
            &dirObjectAttributes,
            &iosb,
            0,
            FILE_ATTRIBUTE_DIRECTORY,
            FILE_SHARE_READ,
            FILE_OPEN,
            FILE_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_ALERT,
            nullptr,
            0);


        if (!NT_SUCCESS(status))
        {
            std::wcerr << "Error " << std::hex << status << " opening Directory " << dirName << std::endl;
            throw CSystemException("directory opening failed", status);
        }

        std::vector<unsigned char> buffer(sizeof(Nt::FILE_FULL_DIR_INFORMATION) * 5000);

        Directory.reserve(1000);

        for (BOOLEAN RestartScan = TRUE ; ; RestartScan = FALSE)
        {
            IO_STATUS_BLOCK IoStatusBlock;
            const NTSTATUS status = g_nt.NtQueryDirectoryFile(
                hFile,
                nullptr,                          // optional event
                nullptr,                          // ApcRoutine
                nullptr,                          // ApcRoutine context
                &IoStatusBlock,
                &buffer[0],
                static_cast<ULONG>(buffer.size()),
                Nt::FileFullDirectoryInformation,  // full entry info
                FALSE,                         // ReturnSingleEntry
                nullptr,                       // FileName
                RestartScan                    // first or continuous batch
                );

            if (!NT_SUCCESS(status))
            {
                if (status == STATUS_BUFFER_OVERFLOW)
                {
                    buffer.resize(2 * buffer.size());
                    continue;
                }

                if (status == STATUS_NO_MORE_FILES || status == STATUS_NO_SUCH_FILE)
                {
                    break;
                }

                std::wcerr << "Error " << std::hex << status << " querying Directory " << dirName << std::endl;
                throw CSystemException("directory querying failed", status);
            }

            Nt::FILE_FULL_DIR_INFORMATION * DirInfo = reinterpret_cast<Nt::FILE_FULL_DIR_INFORMATION*>(&buffer[0]);

            for (bool done=true; done; )
            {
                if (DirInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                {
                    const ULONG NameLength = DirInfo->FileNameLength;
                    const bool spec = (NameLength == 2 && 0 == memcmp(DirInfo->FileName, L".",  2))
                        || (NameLength == 4 && 0 == memcmp(DirInfo->FileName, L"..", 4));
                    if (!spec)
                    {
                        Directory.emplace_back( CDirectoryEntryPtr(new CDirectory(*DirInfo)) );
                    }
                }
                else
                {
                    Directory.emplace_back( CDirectoryEntryPtr(new CFile(*DirInfo)) );
                }

                done = (DirInfo->NextEntryOffset != 0);
                DirInfo = reinterpret_cast<Nt::FILE_FULL_DIR_INFORMATION*>( reinterpret_cast<PUCHAR>(DirInfo) + DirInfo->NextEntryOffset );
            }
        }

        Directory.shrink_to_fit();
        return;
    }

    void CDirectoryVisitorIterator::PushLevel(const std::wstring & dirName)
    {
        m_Stack.emplace_back( CDirectoryVisitorIteratorLevel() );
        CDirectoryVisitorIteratorLevel & top = m_Stack.back();
        ReadDirectory(dirName, top.m_Dir);
        top.m_Pos = 0;
        top.m_Path = dirName;
        m_Sorter.Sort(top.m_Dir, dirName);
    }

    CDirectoryVisitorIterator::CDirectoryVisitorIterator(const std::wstring& dirName, IDirectorySorter & sorter)
        : m_Sorter(sorter)
    {
        m_Stack.reserve(30); // first levels of directory recursion
        PushLevel(dirName);
    }

    CDirectoryVisitorIterator::~CDirectoryVisitorIterator()
    {
    }

    void CDirectoryVisitorIterator::SkipEmptyLevels()
    {
        while (!m_Stack.empty() && m_Stack.back().m_Pos == m_Stack.back().m_Dir.size())
        {
            m_Stack.pop_back();
            if (!m_Stack.empty())
                m_Stack.back().Next();
        }
    }

    void CDirectoryVisitorIterator::operator ++ ()
    {
        SkipEmptyLevels();

        if (m_Stack.empty())
        {
            return;
        }

        CDirectoryEntryPtr dir = m_Stack.back().m_Dir[m_Stack.back().m_Pos];

        if (dir->IsDirectory())
        {
            std::wstring dirPath = m_Stack.back().m_Path + L"\\" + dir->GetName();
            PushLevel(dirPath);
            if (m_Stack.back().m_Dir.empty())
            {
                m_Stack.pop_back();
                m_Stack.back().Next();
            }
            else
            {
                return;
            }
        }
        else
        {
            m_Stack.back().Next();
        }

        SkipEmptyLevels();
    }

    CDirectoryEntryPtr CDirectoryVisitorIterator::operator -> ()
    {
        return m_Stack.back().m_Dir[ m_Stack.back().m_Pos ];
    }

    const std::wstring & CDirectoryVisitorIterator::GetPath() const
    {
        return m_Stack.back().m_Path;
    }

    bool CDirectoryVisitorIterator::IsValid() const
    {
        return !m_Stack.empty();
    }

    CDirectoryVisitorIterator::operator bool () const
    {
        return IsValid();
    }

}
