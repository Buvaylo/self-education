#pragma once
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include <winternl.h>

#pragma warning( push )
#pragma warning( disable : 4005 )
#include <ntstatus.h>
#pragma warning( pop )

#define NT_SUCCESS(status) ((NTSTATUS)(status) >= 0)

#define WINAPI_PREFIX L"\\\\?\\" 
#define NT_PREFIX L"\\??\\"
#define NT_UNC_PREFIX L"\\??\\UNC\\"
#define WINAPI_UNC_PREFIX L"\\\\"


#define FILE_READ_DATA            ( 0x0001 )    // file & pipe 
#define FILE_LIST_DIRECTORY       ( 0x0001 )    // directory 
 
#define FILE_WRITE_DATA           ( 0x0002 )    // file & pipe 
#define FILE_ADD_FILE             ( 0x0002 )    // directory 
 
#define FILE_APPEND_DATA          ( 0x0004 )    // file 
#define FILE_ADD_SUBDIRECTORY     ( 0x0004 )    // directory 
#define FILE_CREATE_PIPE_INSTANCE ( 0x0004 )    // named pipe 
 
#define FILE_READ_EA              ( 0x0008 )    // file & directory 
 
#define FILE_WRITE_EA             ( 0x0010 )    // file & directory 
 
#define FILE_EXECUTE              ( 0x0020 )    // file 
#define FILE_TRAVERSE             ( 0x0020 )    // directory 
 
#define FILE_DELETE_CHILD         ( 0x0040 )    // directory 
 
#define FILE_READ_ATTRIBUTES      ( 0x0080 )    // all 
 
#define FILE_WRITE_ATTRIBUTES     ( 0x0100 )    // all 

#define FILE_SUPERSEDE                  0x00000000 
#define FILE_OPEN                       0x00000001 
#define FILE_CREATE                     0x00000002 
#define FILE_OPEN_IF                    0x00000003 
#define FILE_OVERWRITE                  0x00000004 
#define FILE_OVERWRITE_IF               0x00000005 
#define FILE_MAXIMUM_DISPOSITION        0x00000005 

#define FILE_DIRECTORY_FILE                     0x00000001 
#define FILE_WRITE_THROUGH                      0x00000002 
#define FILE_SEQUENTIAL_ONLY                    0x00000004 
#define FILE_NO_INTERMEDIATE_BUFFERING          0x00000008 
 
#define FILE_SYNCHRONOUS_IO_ALERT               0x00000010 
#define FILE_SYNCHRONOUS_IO_NONALERT            0x00000020 
#define FILE_NON_DIRECTORY_FILE                 0x00000040 
#define FILE_CREATE_TREE_CONNECTION             0x00000080 
 
#define FILE_COMPLETE_IF_OPLOCKED               0x00000100 
#define FILE_NO_EA_KNOWLEDGE                    0x00000200 
//UNUSED                                        0x00000400 
#define FILE_RANDOM_ACCESS                      0x00000800 
 
#define FILE_DELETE_ON_CLOSE                    0x00001000 
#define FILE_OPEN_BY_FILE_ID                    0x00002000 
#define FILE_OPEN_FOR_BACKUP_INTENT             0x00004000 
#define FILE_NO_COMPRESSION                     0x00008000 

#define OBJ_INHERIT 2L
#define OBJ_PERMANENT 16L
#define OBJ_EXCLUSIVE 32L
#define OBJ_CASE_INSENSITIVE 64L
#define OBJ_OPENIF 128L
#define OBJ_OPENLINK 256L
#define OBJ_VALID_ATTRIBUTES 498L

#define InitializeObjectAttributes(p,n,a,r,s) { \
  (p)->Length = sizeof(OBJECT_ATTRIBUTES); \
  (p)->RootDirectory = (r); \
  (p)->Attributes = (a); \
  (p)->ObjectName = (n); \
  (p)->SecurityDescriptor = (s); \
  (p)->SecurityQualityOfService = NULL; \
}

#ifndef RTL_CONSTANT_STRING
#define RTL_CONSTANT_STRING(x) { (USHORT)wcslen(x) * sizeof(wchar_t), (USHORT)wcslen(x) * sizeof(wchar_t), x }
#endif

#ifndef RTL_CONSTANT_OBJECT_ATTRIBUTES
#define RTL_CONSTANT_OBJECT_ATTRIBUTES(r, o, a) { sizeof(OBJECT_ATTRIBUTES), r, o, a, NULL, NULL }
#endif

#define _Field_size_bytes_(field)

//	Note that the following definitions are undocumented altogether, or are documented only in WDK files that do not co-exist with usermode SDK files
namespace Nt
{
    typedef struct _KEY_BASIC_INFORMATION {
        LARGE_INTEGER LastWriteTime;
        ULONG   TitleIndex;
        ULONG   NameLength;
        WCHAR   Name[1];            // Variable length string
    } KEY_BASIC_INFORMATION, *PKEY_BASIC_INFORMATION;

    typedef struct _KEY_NODE_INFORMATION {
        LARGE_INTEGER LastWriteTime;
        ULONG   TitleIndex;
        ULONG   ClassOffset;
        ULONG   ClassLength;
        ULONG   NameLength;
        WCHAR   Name[1];            // Variable length string
    //          Class[1];           // Variable length string not declared
    } KEY_NODE_INFORMATION, *PKEY_NODE_INFORMATION;

    typedef struct _KEY_FULL_INFORMATION {
        LARGE_INTEGER LastWriteTime;
        ULONG   TitleIndex;
        ULONG   ClassOffset;
        ULONG   ClassLength;
        ULONG   SubKeys;
        ULONG   MaxNameLen;
        ULONG   MaxClassLen;
        ULONG   Values;
        ULONG   MaxValueNameLen;
        ULONG   MaxValueDataLen;
        WCHAR   Class[1];           // Variable length
    } KEY_FULL_INFORMATION, *PKEY_FULL_INFORMATION;

    typedef enum _KEY_INFORMATION_CLASS {
        KeyBasicInformation,
        KeyNodeInformation,
        KeyFullInformation,
        KeyNameInformation,
        KeyCachedInformation,
        KeyFlagsInformation,
        KeyVirtualizationInformation,
        KeyHandleTagsInformation,
        KeyTrustInformation,
        MaxKeyInfoClass  // MaxKeyInfoClass should always be the last enum
    } KEY_INFORMATION_CLASS;

    typedef struct _KEY_VALUE_BASIC_INFORMATION {
        ULONG   TitleIndex;
        ULONG   Type;
        ULONG   NameLength;
        WCHAR   Name[1];            // Variable size
    } KEY_VALUE_BASIC_INFORMATION, *PKEY_VALUE_BASIC_INFORMATION;

    typedef struct _KEY_VALUE_FULL_INFORMATION {
        ULONG   TitleIndex;
        ULONG   Type;
        ULONG   DataOffset;
        ULONG   DataLength;
        ULONG   NameLength;
        WCHAR   Name[1];            // Variable size
    //          Data[1];            // Variable size data not declared
    } KEY_VALUE_FULL_INFORMATION, *PKEY_VALUE_FULL_INFORMATION;

    typedef struct _KEY_VALUE_PARTIAL_INFORMATION {
        ULONG   TitleIndex;
        ULONG   Type;
        ULONG   DataLength;
        _Field_size_bytes_(DataLength) UCHAR Data[1]; // Variable size
    } KEY_VALUE_PARTIAL_INFORMATION, *PKEY_VALUE_PARTIAL_INFORMATION;

    typedef struct _KEY_VALUE_PARTIAL_INFORMATION_ALIGN64 {
        ULONG   Type;
        ULONG   DataLength;
        _Field_size_bytes_(DataLength) UCHAR   Data[1];            // Variable size
    } KEY_VALUE_PARTIAL_INFORMATION_ALIGN64, *PKEY_VALUE_PARTIAL_INFORMATION_ALIGN64;

    typedef enum _KEY_VALUE_INFORMATION_CLASS {
        KeyValueBasicInformation,
        KeyValueFullInformation,
        KeyValuePartialInformation,
        KeyValueFullInformationAlign64,
        KeyValuePartialInformationAlign64,
        MaxKeyValueInfoClass  // MaxKeyValueInfoClass should always be the last enum
    } KEY_VALUE_INFORMATION_CLASS;

    typedef enum _FILE_INFORMATION_CLASS {
        FileDirectoryInformation = 1,
        FileFullDirectoryInformation,   // 2
        FileBothDirectoryInformation,   // 3
        FileBasicInformation,           // 4
        FileStandardInformation,        // 5
        FileInternalInformation,        // 6
        FileEaInformation,              // 7
        FileAccessInformation,          // 8
        FileNameInformation,            // 9
        FileRenameInformation,          // 10
        FileLinkInformation,            // 11
        FileNamesInformation,           // 12
        FileDispositionInformation,     // 13
        FilePositionInformation,        // 14
        FileFullEaInformation,          // 15
        FileModeInformation,            // 16
        FileAlignmentInformation,       // 17
        FileAllInformation,             // 18
        FileAllocationInformation,      // 19
        FileEndOfFileInformation,       // 20
        FileAlternateNameInformation,   // 21
        FileStreamInformation,          // 22
        FilePipeInformation,            // 23
        FilePipeLocalInformation,       // 24
        FilePipeRemoteInformation,      // 25
        FileMailslotQueryInformation,   // 26
        FileMailslotSetInformation,     // 27
        FileCompressionInformation,     // 28
        FileObjectIdInformation,        // 29
        FileCompletionInformation,      // 30
        FileMoveClusterInformation,     // 31
        FileQuotaInformation,           // 32
        FileReparsePointInformation,    // 33
        FileNetworkOpenInformation,     // 34
        FileAttributeTagInformation,    // 35
        FileTrackingInformation,        // 36
        FileIdBothDirectoryInformation, // 37
        FileIdFullDirectoryInformation, // 38
        FileValidDataLengthInformation, // 39
        FileShortNameInformation,       // 40
        FileIoCompletionNotificationInformation, // 41
        FileIoStatusBlockRangeInformation,       // 42
        FileIoPriorityHintInformation,           // 43
        FileSfioReserveInformation,              // 44
        FileSfioVolumeInformation,               // 45
        FileHardLinkInformation,                 // 46
        FileProcessIdsUsingFileInformation,      // 47
        FileNormalizedNameInformation,           // 48
        FileNetworkPhysicalNameInformation,      // 49
        FileIdGlobalTxDirectoryInformation,      // 50
        FileIsRemoteDeviceInformation,           // 51
        FileUnusedInformation,                   // 52
        FileNumaNodeInformation,                 // 53
        FileStandardLinkInformation,             // 54
        FileRemoteProtocolInformation,           // 55
        FileRenameInformationBypassAccessCheck,  // 56
        FileLinkInformationBypassAccessCheck,    // 57
        FileVolumeNameInformation,               // 58
        FileIdInformation,                       // 59
        FileIdExtdDirectoryInformation,          // 60
        FileReplaceCompletionInformation,        // 61
        FileHardLinkFullIdInformation,           // 62
        FileIdExtdBothDirectoryInformation,      // 63
        FileDispositionInformationEx,            // 64
        FileRenameInformationEx,                 // 65
        FileRenameInformationExBypassAccessCheck, // 66
        FileMaximumInformation
    } FILE_INFORMATION_CLASS, *PFILE_INFORMATION_CLASS;

    typedef struct _FILE_DIRECTORY_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        WCHAR FileName[1];
    } FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

    typedef struct _FILE_FULL_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        WCHAR FileName[1];
    } FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;

    typedef struct _FILE_ID_FULL_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        LARGE_INTEGER FileId;
        WCHAR FileName[1];
    } FILE_ID_FULL_DIR_INFORMATION, *PFILE_ID_FULL_DIR_INFORMATION;

    typedef struct _FILE_BOTH_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        CCHAR ShortNameLength;
        WCHAR ShortName[12];
        WCHAR FileName[1];
    } FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;

    typedef struct _FILE_ID_BOTH_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        CCHAR ShortNameLength;
        WCHAR ShortName[12];
        LARGE_INTEGER FileId;
        WCHAR FileName[1];
    } FILE_ID_BOTH_DIR_INFORMATION, *PFILE_ID_BOTH_DIR_INFORMATION;

    typedef struct _FILE_NAMES_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        ULONG FileNameLength;
        WCHAR FileName[1];
    } FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;

    typedef struct _FILE_ID_GLOBAL_TX_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        LARGE_INTEGER FileId;
        GUID LockingTransactionId;
        ULONG TxInfoFlags;
        WCHAR FileName[1];
    } FILE_ID_GLOBAL_TX_DIR_INFORMATION, *PFILE_ID_GLOBAL_TX_DIR_INFORMATION;

#define FILE_ID_GLOBAL_TX_DIR_INFO_FLAG_WRITELOCKED         0x00000001
#define FILE_ID_GLOBAL_TX_DIR_INFO_FLAG_VISIBLE_TO_TX       0x00000002
#define FILE_ID_GLOBAL_TX_DIR_INFO_FLAG_VISIBLE_OUTSIDE_TX  0x00000004

    typedef struct _EXT_FILE_ID_128 {
       BYTE Identifier[16];
    } EXT_FILE_ID_128, FILE_ID_128, *PEXT_FILE_ID_128;

    typedef struct _FILE_ID_EXTD_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        ULONG ReparsePointTag;
        FILE_ID_128 FileId;
        WCHAR FileName[1];
    } FILE_ID_EXTD_DIR_INFORMATION, *PFILE_ID_EXTD_DIR_INFORMATION;

    typedef struct _FILE_ID_EXTD_BOTH_DIR_INFORMATION {
        ULONG NextEntryOffset;
        ULONG FileIndex;
        LARGE_INTEGER CreationTime;
        LARGE_INTEGER LastAccessTime;
        LARGE_INTEGER LastWriteTime;
        LARGE_INTEGER ChangeTime;
        LARGE_INTEGER EndOfFile;
        LARGE_INTEGER AllocationSize;
        ULONG FileAttributes;
        ULONG FileNameLength;
        ULONG EaSize;
        ULONG ReparsePointTag;
        FILE_ID_128 FileId;
        CCHAR ShortNameLength;
        WCHAR ShortName[12];
        WCHAR FileName[1];
    } FILE_ID_EXTD_BOTH_DIR_INFORMATION, *PFILE_ID_EXTD_BOTH_DIR_INFORMATION;

    typedef struct _FILE_OBJECTID_INFORMATION {
        LONGLONG FileReference;
        UCHAR ObjectId[16];
        union {
            struct {
                UCHAR BirthVolumeId[16];
                UCHAR BirthObjectId[16];
                UCHAR DomainId[16];
            } DUMMYSTRUCTNAME;
            UCHAR ExtendedInfo[48];
        } DUMMYUNIONNAME;
    } FILE_OBJECTID_INFORMATION, *PFILE_OBJECTID_INFORMATION;

    typedef enum _OBJECT_INFORMATION_CLASS {
        ObjectBasicInformation,
        ObjectNameInformation,
        ObjectTypeInformation,
        ObjectAllInformation,
        ObjectDataInformation
    } OBJECT_INFORMATION_CLASS, *POBJECT_INFORMATION_CLASS;

    typedef struct _WNF_STATE_NAME {
        ULONG Data[2];
    } WNF_STATE_NAME, *PWNF_STATE_NAME;

    typedef const PWNF_STATE_NAME PCWNF_STATE_NAME;

    typedef enum _WNF_STATE_NAME_LIFETIME
    {
        WnfWellKnownStateName,
        WnfPermanentStateName,
        WnfPersistentStateName,
        WnfTemporaryStateName
    } WNF_STATE_NAME_LIFETIME;

    typedef enum _WNF_STATE_NAME_INFORMATION
    {
        WnfInfoStateNameExist,
        WnfInfoSubscribersPresent,
        WnfInfoIsQuiescent
    } WNF_STATE_NAME_INFORMATION;

    typedef enum _WNF_DATA_SCOPE
    {
        WnfDataScopeSystem,
        WnfDataScopeSession,
        WnfDataScopeUser,
        WnfDataScopeProcess
    } WNF_DATA_SCOPE;

    typedef struct _WNF_TYPE_ID
    {
        GUID TypeId;
    } WNF_TYPE_ID, *PWNF_TYPE_ID;

    typedef const WNF_TYPE_ID *PCWNF_TYPE_ID;

    // rev
    typedef ULONG WNF_CHANGE_STAMP, *PWNF_CHANGE_STAMP;


    //function pointers prototypes
    typedef NTSTATUS(NTAPI *PFN_NtClose)(
        _In_ HANDLE Handle
        );

    typedef NTSTATUS(NTAPI *PFN_NtCreateFile)(
        _Out_ PHANDLE FileHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _Out_ PIO_STATUS_BLOCK IoStatusBlock,
        _In_opt_ PLARGE_INTEGER AllocationSize,
        _In_ ULONG FileAttributes,
        _In_ ULONG ShareAccess,
        _In_ ULONG CreateDisposition,
        _In_ ULONG CreateOptions,
        _In_ PVOID EaBuffer,
        _In_ ULONG EaLength
        );

    typedef NTSTATUS (NTAPI *PFN_NtReadFile)(
        HANDLE                  FileHandle,
        HANDLE                  Event,
        PIO_APC_ROUTINE         ApcRoutine,
        PVOID                   ApcContext,
        PIO_STATUS_BLOCK        IoStatusBlock,
        PVOID                   Buffer,
        ULONG                   Length,
        PLARGE_INTEGER          ByteOffset,
        PULONG                  Key
        );

    typedef NTSTATUS (NTAPI *PFN_NtWriteFile)(
        HANDLE                  FileHandle,
        HANDLE                  Event,
        PIO_APC_ROUTINE         ApcRoutine,
        PVOID                   ApcContext,
        PIO_STATUS_BLOCK        IoStatusBlock,
        LPCVOID                 Buffer,
        ULONG                   Length,
        PLARGE_INTEGER          ByteOffset,
        PULONG                  Key
        );

    typedef NTSTATUS(NTAPI *PFN_NtCreateKey)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _In_ ULONG TitleIndex,
        _In_opt_ PUNICODE_STRING Class,
        _In_ ULONG CreateOptions,
        _Out_opt_ PULONG Disposition
        );

    typedef NTSTATUS(NTAPI *PFN_NtCreateKeyTransacted)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _In_ ULONG TitleIndex,
        _In_opt_ PUNICODE_STRING Class,
        _In_ ULONG CreateOptions,
        _In_ HANDLE TransactionHandle,
        _Out_opt_ PULONG Disposition
        );

    typedef NTSTATUS(NTAPI *PFN_NtCreateWnfStateName)(
        _Out_ PWNF_STATE_NAME StateName,
        _In_ WNF_STATE_NAME_LIFETIME NameLifetime,
        _In_ WNF_DATA_SCOPE DataScope,
        _In_ BOOLEAN PersistData,
        _In_opt_ PCWNF_TYPE_ID TypeId,
        _In_ ULONG MaximumStateSize,
        _In_ PSECURITY_DESCRIPTOR SecurityDescriptor
        );

    typedef NTSTATUS(NTAPI *PFN_NtDeleteKey)(
        _In_ HANDLE KeyHandle
        );

    typedef NTSTATUS(NTAPI *PFN_NtDeleteFile)(
        _In_ POBJECT_ATTRIBUTES ObjectAttributes
        );
    
    typedef NTSTATUS(NTAPI *PFN_NtDeleteValueKey)(
        _In_ HANDLE KeyHandle,
        _In_ PUNICODE_STRING ValueName
        );

    typedef NTSTATUS(NTAPI* PFN_NtDeleteWnfStateName)(
        _In_ PCWNF_STATE_NAME StateName
        );

    typedef NTSTATUS(NTAPI *PFN_NtEnumerateKey)(
        _In_ HANDLE KeyHandle,
        _In_ ULONG Index,
        _In_ KEY_INFORMATION_CLASS KeyInformationClass,
        _Out_opt_ PVOID KeyInformation,
        _In_ ULONG Length,
        _Out_ PULONG ResultLength
        );

    typedef NTSTATUS(NTAPI *PFN_NtEnumerateValueKey)(
        _In_ HANDLE KeyHandle,
        _In_ ULONG Index,
        _In_ KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
        _Out_opt_ PVOID KeyValueInformation,
        _In_ ULONG Length,
        _Out_ PULONG ResultLength
        );

    typedef NTSTATUS(NTAPI *PFN_NtFlushKey)(
        _In_ HANDLE KeyHandle
        );

    typedef NTSTATUS(NTAPI *PFN_NtNotifyChangeKey)(
        _In_ HANDLE KeyHandle,
        _In_opt_ HANDLE Event,
        _In_opt_ PIO_APC_ROUTINE ApcRoutine,
        _In_opt_ PVOID ApcContext,
        _Out_ PIO_STATUS_BLOCK IoStatusBlock,
        _In_ ULONG CompletionFilter,
        _In_ BOOLEAN WatchTree,
        _Out_opt_ PVOID Buffer,
        _In_ ULONG BufferSize,
        _In_ BOOLEAN Asynchronous
        );

    typedef NTSTATUS(NTAPI *PFN_NtOpenFile)(
        _Out_ PHANDLE FileHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _Out_ PIO_STATUS_BLOCK IoStatusBlock,
        _In_ ULONG ShareAccess,
        _In_ ULONG OpenOptions
        );

    typedef NTSTATUS(NTAPI *PFN_NtOpenKey)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes
        );

    typedef NTSTATUS(NTAPI *PFN_NtOpenKeyEx)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _In_ ULONG OpenOptions
        );

    typedef NTSTATUS(NTAPI *PFN_NtOpenKeyTransacted)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _In_ HANDLE TransactionHandle
        );

    typedef NTSTATUS(NTAPI *PFN_NtOpenKeyTransactedEx)(
        _Out_ PHANDLE KeyHandle,
        _In_ ACCESS_MASK DesiredAccess,
        _In_ POBJECT_ATTRIBUTES ObjectAttributes,
        _In_ ULONG OpenOptions,
        _In_ HANDLE TransactionHandle
        );

    typedef NTSTATUS(NTAPI *PFN_NtQueryDirectoryFile)(
        _In_ HANDLE FileHandle,
        _In_opt_ HANDLE Event,
        _In_opt_ PIO_APC_ROUTINE ApcRoutine,
        _In_opt_ PVOID ApcContext,
        _Out_ PIO_STATUS_BLOCK IoStatusBlock,
        _Out_ PVOID FileInformation,
        _In_ ULONG Length,
        _In_ FILE_INFORMATION_CLASS FileInformationClass,
        _In_ BOOLEAN ReturnSingleEntry,
        _In_opt_ PUNICODE_STRING FileName,
        _In_ BOOLEAN RestartScan
        );
    
    typedef NTSTATUS(NTAPI *PFN_NtQueryKey)(
        _In_ HANDLE KeyHandle,
        _In_ KEY_INFORMATION_CLASS KeyInformationClass,
        _Out_opt_ PVOID KeyInformation,
        _In_ ULONG Length,
        _Out_ PULONG ResultLength
        );

    typedef NTSTATUS(NTAPI *PFN_NtQueryObject)(
        _In_opt_ HANDLE Handle,
        _In_ OBJECT_INFORMATION_CLASS ObjectInformationClass,
        _Out_opt_ PVOID ObjectInformation,
        _In_ ULONG ObjectInformationLength,
        _Out_opt_ PULONG ReturnLength
        );

    typedef NTSTATUS(NTAPI *PFN_NtQueryValueKey)(
        _In_ HANDLE KeyHandle,
        _In_ PUNICODE_STRING ValueName,
        _In_ KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
        _Out_opt_ PVOID KeyValueInformation,
        _In_ ULONG Length,
        _Out_ PULONG ResultLength
        );

    typedef NTSTATUS(NTAPI *PFN_NtSetValueKey)(
        _In_ HANDLE KeyHandle,
        _In_ PUNICODE_STRING ValueName,
        _In_opt_ ULONG TitleIndex,
        _In_ ULONG Type,
        _In_opt_ PVOID Data,
        _In_ ULONG DataSize
        );

    typedef VOID(NTAPI *PFN_RtlInitUnicodeString)(
        _In_ PUNICODE_STRING DestinationString,
        _In_ PCWSTR SourceString
        );


    class NtApi
    {
    public:
        NtApi();
        virtual ~NtApi();

        NTSTATUS NTAPI NtClose(
            _In_ HANDLE Handle
        ) const;

        NTSTATUS NTAPI NtCreateFile(
            _Out_ PHANDLE FileHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _In_opt_ PLARGE_INTEGER AllocationSize,
            _In_ ULONG FileAttributes,
            _In_ ULONG ShareAccess,
            _In_ ULONG CreateDisposition,
            _In_ ULONG CreateOptions,
            _In_ PVOID EaBuffer,
            _In_ ULONG EaLength
        ) const;

        NTSTATUS NTAPI NtReadFile(
            _In_ HANDLE FileHandle,
            _In_ HANDLE Event,
            _In_ PIO_APC_ROUTINE ApcRoutine,
            _In_ PVOID ApcContext,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _Out_ PVOID Buffer,
            _In_ ULONG Length,
            _In_ PLARGE_INTEGER ByteOffset,
            _Out_ PULONG Key
        ) const;

        NTSTATUS NTAPI NtWriteFile(
            _In_ HANDLE FileHandle,
            _In_ HANDLE Event,
            _In_ PIO_APC_ROUTINE ApcRoutine,
            _In_ PVOID ApcContext,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _In_ LPCVOID Buffer,
            _In_ ULONG Length,
            _In_ PLARGE_INTEGER ByteOffset,
            _Out_ PULONG Key
        ) const;

        NTSTATUS NTAPI NtCreateKey(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _In_ ULONG TitleIndex,
            _In_opt_ PUNICODE_STRING Class,
            _In_ ULONG CreateOptions,
            _Out_opt_ PULONG Disposition
        ) const;

        NTSTATUS NTAPI NtCreateKeyTransacted(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _In_ ULONG TitleIndex,
            _In_opt_ PUNICODE_STRING Class,
            _In_ ULONG CreateOptions,
            _In_ HANDLE TransactionHandle,
            _Out_opt_ PULONG Disposition
        ) const;

        NTSTATUS NTAPI NtCreateWnfStateName(
            _Out_ PWNF_STATE_NAME StateName,
            _In_ WNF_STATE_NAME_LIFETIME NameLifetime,
            _In_ WNF_DATA_SCOPE DataScope,
            _In_ BOOLEAN PersistData,
            _In_opt_ PCWNF_TYPE_ID TypeId,
            _In_ ULONG MaximumStateSize,
            _In_ PSECURITY_DESCRIPTOR SecurityDescriptor
        ) const;

        NTSTATUS NTAPI NtDeleteKey(
            _In_ HANDLE KeyHandle
        ) const;

        NTSTATUS NTAPI NtDeleteFile(
            _In_ POBJECT_ATTRIBUTES ObjectAttributes
        ) const;

        NTSTATUS NTAPI NtDeleteValueKey(
            _In_ HANDLE KeyHandle,
            _In_ PUNICODE_STRING ValueName
        ) const;

        NTSTATUS NTAPI NtDeleteWnfStateName(
            _In_ PCWNF_STATE_NAME StateName
        ) const;

        NTSTATUS NTAPI NtEnumerateKey(
            _In_ HANDLE KeyHandle,
            _In_ ULONG Index,
            _In_ KEY_INFORMATION_CLASS KeyInformationClass,
            _Out_opt_ PVOID KeyInformation,
            _In_ ULONG Length,
            _Out_ PULONG ResultLength
        ) const;

        NTSTATUS NTAPI NtEnumerateValueKey(
            _In_ HANDLE KeyHandle,
            _In_ ULONG Index,
            _In_ KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
            _Out_opt_ PVOID KeyValueInformation,
            _In_ ULONG Length,
            _Out_ PULONG ResultLength
        ) const;

        NTSTATUS NTAPI NtFlushKey(
            _In_ HANDLE KeyHandle
        ) const;

        NTSTATUS NTAPI NtNotifyChangeKey(
            _In_ HANDLE KeyHandle,
            _In_opt_ HANDLE Event,
            _In_opt_ PIO_APC_ROUTINE ApcRoutine,
            _In_opt_ PVOID ApcContext,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _In_ ULONG CompletionFilter,
            _In_ BOOLEAN WatchTree,
            _Out_opt_ PVOID Buffer,
            _In_ ULONG BufferSize,
            _In_ BOOLEAN Asynchronous
        ) const;

        NTSTATUS NTAPI NtOpenFile(
            _Out_ PHANDLE FileHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _In_ ULONG ShareAccess,
            _In_ ULONG OpenOptions
        ) const;

        NTSTATUS NTAPI NtOpenKey(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes
        ) const;

        NTSTATUS NTAPI NtOpenKeyEx(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _In_ ULONG OpenOptions
        ) const;

        NTSTATUS NTAPI NtOpenKeyTransacted(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _In_ HANDLE TransactionHandle
        ) const;

        NTSTATUS NTAPI NtOpenKeyTransactedEx(
            _Out_ PHANDLE KeyHandle,
            _In_ ACCESS_MASK DesiredAccess,
            _In_ POBJECT_ATTRIBUTES ObjectAttributes,
            _In_ ULONG OpenOptions,
            _In_ HANDLE TransactionHandle
        ) const;

        NTSTATUS NTAPI NtQueryDirectoryFile(
            _In_ HANDLE FileHandle,
            _In_opt_ HANDLE Event,
            _In_opt_ PIO_APC_ROUTINE ApcRoutine,
            _In_opt_ PVOID ApcContext,
            _Out_ PIO_STATUS_BLOCK IoStatusBlock,
            _Out_ PVOID FileInformation,
            _In_ ULONG Length,
            _In_ FILE_INFORMATION_CLASS FileInformationClass,
            _In_ BOOLEAN ReturnSingleEntry,
            _In_opt_ PUNICODE_STRING FileName,
            _In_ BOOLEAN RestartScan
        ) const;

        NTSTATUS NTAPI NtQueryKey(
            _In_ HANDLE KeyHandle,
            _In_ KEY_INFORMATION_CLASS KeyInformationClass,
            _Out_opt_ PVOID KeyInformation,
            _In_ ULONG Length,
            _Out_ PULONG ResultLength
        ) const;

        NTSTATUS NTAPI NtQueryObject(
            _In_opt_ HANDLE Handle,
            _In_ OBJECT_INFORMATION_CLASS ObjectInformationClass,
            _Out_opt_ PVOID ObjectInformation,
            _In_ ULONG ObjectInformationLength,
            _Out_opt_ PULONG ReturnLength
        ) const;

        NTSTATUS NTAPI NtQueryValueKey(
            _In_ HANDLE KeyHandle,
            _In_ PUNICODE_STRING ValueName,
            _In_ KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
            _Out_opt_ PVOID KeyValueInformation,
            _In_ ULONG Length,
            _Out_ PULONG ResultLength
        ) const;

        NTSTATUS NTAPI NtSetValueKey(
            _In_ HANDLE KeyHandle,
            _In_ PUNICODE_STRING ValueName,
            _In_opt_ ULONG TitleIndex,
            _In_ ULONG Type,
            _In_opt_ PVOID Data,
            _In_ ULONG DataSize
        ) const;

        VOID NTAPI RtlInitUnicodeString(
            _In_ PUNICODE_STRING DestinationString,
            _In_ PCWSTR SourceString
        ) const;

        // NtNotifyChangeDirectoryFile
        // NtNotifyChangeMultipleKeys
        // NtQueryInformationFile
        // NtRenameKey
        // NtSetInformationFile
        // NtSetInformationKey


    private:
        NtApi(const NtApi&);
        NtApi & operator = (const NtApi&);

    private:
        HMODULE m_hLib;
        PFN_NtClose m_pfnNtClose;
        PFN_NtCreateFile m_pfnNtCreateFile;
        PFN_NtReadFile m_pfnNtReadFile;
        PFN_NtWriteFile m_pfnNtWriteFile;
        PFN_NtCreateKey m_pfnNtCreateKey;
        PFN_NtCreateKeyTransacted m_pfnNtCreateKeyTransacted;
        PFN_NtCreateWnfStateName m_pfnNtCreateWnfStateName;
        PFN_NtDeleteKey m_pfnNtDeleteKey;
        PFN_NtDeleteFile m_pfnNtDeleteFile;
        PFN_NtDeleteValueKey m_pfnNtDeleteValueKey;
        PFN_NtDeleteWnfStateName m_pfnNtDeleteWnfStateName;
        PFN_NtEnumerateKey m_pfnNtEnumerateKey;
        PFN_NtEnumerateValueKey m_pfnNtEnumerateValueKey;
        PFN_NtFlushKey m_pfnNtFlushKey;
        PFN_NtNotifyChangeKey m_pfnNtNotifyChangeKey;
        PFN_NtOpenFile m_pfnNtOpenFile;
        PFN_NtOpenKey m_pfnNtOpenKey;
        PFN_NtOpenKeyEx m_pfnNtOpenKeyEx;
        PFN_NtOpenKeyTransacted m_pfnNtOpenKeyTransacted;
        PFN_NtOpenKeyTransactedEx m_pfnNtOpenKeyTransactedEx;
        PFN_NtQueryDirectoryFile m_pfnNtQueryDirectoryFile;
        PFN_NtQueryKey m_pfnNtQueryKey;
        PFN_NtQueryObject m_pfnNtQueryObject;
        PFN_NtQueryValueKey m_pfnNtQueryValueKey;
        PFN_NtSetValueKey m_pfnNtSetValueKey;
        PFN_RtlInitUnicodeString m_pfnRtlInitUnicodeString;
    };

}

extern const Nt::NtApi g_nt;

namespace Nt
{
    class CFileHandle
    {
    public:
        CFileHandle(HANDLE aHandle = NULL): m_Handle(aHandle) {}
        ~CFileHandle() 
        {
            CloseHandle();
        }

        operator HANDLE() { return m_Handle; }

        HANDLE* operator & ()
        {
            CloseHandle();
            return &m_Handle;
        }

        HANDLE operator = (HANDLE NewHandle) 
        {
            if (m_Handle != NewHandle) 
            {
                if (m_Handle != NULL) 
                {
                    g_nt.NtClose(m_Handle);
                }
                m_Handle = NewHandle;
            }
            return m_Handle;
        }

        HANDLE release() 
        {
            HANDLE temp = m_Handle;
            m_Handle = NULL;
            return temp;
        }

        void CloseHandle()
        {
            if (m_Handle != NULL) 
            {
                g_nt.NtClose(m_Handle);
                m_Handle = NULL;
            }
        }

    private:
        CFileHandle(const CFileHandle&);
        CFileHandle & operator = (const CFileHandle&);

    private:
        HANDLE    m_Handle;
    };

    extern double NTAPI GetSecs();

}
