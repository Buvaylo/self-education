#pragma once

#include "ntapi.h"
#include "DirectoryEntryInterface.h"
#include "DirectorySorter.h"

namespace FileSystemUtils
{
    class CDirectoryVisitorIteratorLevel
    {
        friend class CDirectoryVisitorIterator;
        public:
            void Next() { ++m_Pos; }

            CDirectoryVisitorIteratorLevel()
            {
            }

            CDirectoryVisitorIteratorLevel(CDirectoryVisitorIteratorLevel&& r)
            {
                m_Path.swap(r.m_Path);
                m_Dir.swap(r.m_Dir);
                m_Pos = r.m_Pos;
            }

            CDirectoryVisitorIteratorLevel& operator=(CDirectoryVisitorIteratorLevel&& r)
            {
                m_Path.swap(r.m_Path);
                m_Dir.swap(r.m_Dir);
                m_Pos = r.m_Pos;
                return *this;
            }

        private:
            std::vector<CDirectoryEntryPtr>  m_Dir;
            size_t                           m_Pos;
            std::wstring                     m_Path;
    };

    class CDirectoryVisitorIterator
    {
        public:
            CDirectoryVisitorIterator(const std::wstring& p, IDirectorySorter & sorter);
            ~CDirectoryVisitorIterator();

            void operator ++ ();
            const std::wstring & GetPath() const;

            CDirectoryEntryPtr operator -> ();
            bool IsValid() const;
            operator bool () const;

        private:
            CDirectoryVisitorIterator(const CDirectoryVisitorIterator&) = delete;
            const CDirectoryVisitorIterator& operator=(const CDirectoryVisitorIterator&) = delete;

            void PushLevel(const std::wstring & dirName);
            void SkipEmptyLevels();

        private:
            std::vector<CDirectoryVisitorIteratorLevel>  m_Stack;
            IDirectorySorter &                           m_Sorter;
    };

}
