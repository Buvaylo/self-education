#include "stdafx.h"
#include "ntapi.h"
#include "DirectoryWorkerPrinter.h"

namespace FileSystemUtils
{

    CDirectoryWorkerPrinter::CDirectoryWorkerPrinter()
    {
    }

    CDirectoryWorkerPrinter::~CDirectoryWorkerPrinter()
    {
    }

    IDirectoryWorker::EStatus CDirectoryWorkerPrinter::DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any method for directory node output
        wprintf( L"F %9I64u %9I64u %ws\\%ws\n",
            dirEntry.GetFileSize(),
            dirEntry.GetAllocationSize(),
            parent.c_str(),
            dirEntry.GetName().c_str()
            );
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerPrinter::DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerPrinter::DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any payload for directory visiting BEFORE its content
        wprintf( L"D %9I64u %9I64u %ws\\%ws\n",
            dirEntry.GetFileSize(),
            dirEntry.GetAllocationSize(),
            parent.c_str(),
            dirEntry.GetName().c_str()
           );
        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus CDirectoryWorkerPrinter::DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent)
    {
        // implement here any payload for directory visiting AFTER its content
        return IDirectoryWorker::ContinueTraversing;
    }

}
