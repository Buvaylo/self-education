#include "stdafx.h"
#include "DirectoryIterator.h"
#include "DirectoryTraverser.h"

namespace FileSystemUtils
{
    void CDirectoryIterator::PushLevel(const std::wstring & dirName)
    {
        m_Stack.emplace_back( CDirectoryIteratorLevel() );
        CDirectoryIteratorLevel & top = m_Stack.back();
        ReadDirectory(dirName, top.m_Dir);
        top.m_Pos = 0;
        top.m_Path = dirName;
        m_Sorter.Sort(top.m_Dir, dirName);
    }

    CDirectoryIterator::CDirectoryIterator(const std::wstring& dirName, IDirectorySorter & sorter)
        : m_Sorter(sorter)
    {
        m_Stack.reserve(30);
        PushLevel(dirName);
    }

    CDirectoryIterator::~CDirectoryIterator()
    {
    }

    void CDirectoryIterator::SkipEmptyLevels()
    {
        while (!m_Stack.empty() && m_Stack.back().m_Pos == m_Stack.back().m_Dir.size())
        {
            m_Stack.pop_back();
            if (!m_Stack.empty())
                m_Stack.back().Next();
        }
    }

    void CDirectoryIterator::operator ++ ()
    {
        SkipEmptyLevels();

        if (m_Stack.empty())
        {
            return;
        }

        CDirectoryEntry& dir = m_Stack.back().m_Dir[m_Stack.back().m_Pos];

        if (dir.IsDirectory())
        {
            std::wstring dirPath = m_Stack.back().m_Path + L"\\" + dir.GetName();
            PushLevel(dirPath);
            if (m_Stack.back().m_Dir.empty())
            {
                m_Stack.pop_back();
                m_Stack.back().Next();
            }
            else
            {
                return;
            }
        }
        else
        {
            m_Stack.back().Next();
        }

        SkipEmptyLevels();
    }

    CDirectoryEntry * CDirectoryIterator::operator -> ()
    {
        return & m_Stack.back().m_Dir[ m_Stack.back().m_Pos ];
    }

    const std::wstring & CDirectoryIterator::GetPath() const
    {
        return m_Stack.back().m_Path;
    }

    bool CDirectoryIterator::IsValid() const
    {
        return !m_Stack.empty();
    }

    CDirectoryIterator::operator bool () const
    {
        return IsValid();
    }

}

