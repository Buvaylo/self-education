#pragma once

#include "ntapi.h"
#include "DirectoryEntryInterface.h"

namespace FileSystemUtils
{
    class CDirectory;
    class CFile;

    class CDirectoryVisitorInterface
    {
    public:
        virtual void visit(CDirectory& ref, const std::wstring & parent) = 0;
        virtual void visit(CFile& ref, const std::wstring & parent) = 0;
    };

    // directory entry derivatives
    class CDirectory : public CDirectoryEntryInterface
    {
    public:
        ~CDirectory() {};
        void accept(CDirectoryVisitorInterface& v, const std::wstring & parent) override {  v.visit(*this, parent);  }
        explicit CDirectory(const Nt::FILE_FULL_DIR_INFORMATION & dirInfo) : CDirectoryEntryInterface(dirInfo) {}
    };

    class CFile : public CDirectoryEntryInterface
    {
    public:
        ~CFile() {};
        void accept(CDirectoryVisitorInterface& v, const std::wstring & parent) override {  v.visit(*this, parent); }
        explicit CFile(const Nt::FILE_FULL_DIR_INFORMATION & dirInfo) : CDirectoryEntryInterface(dirInfo) {}
    };

    void RunDirectoryVisitorIterator(const std::wstring & dirName);
}
