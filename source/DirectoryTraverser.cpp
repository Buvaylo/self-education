#include "stdafx.h"
#include "DirectoryTraverser.h"
#include "DirectoryEntry.h"
#include "SystemException.h"

namespace FileSystemUtils
{
    void ReadDirectory(const std::wstring & dirName, std::vector<CDirectoryEntry>& Directory)
    {
        std::wstring ntDirName = NT_PREFIX + dirName;
        UNICODE_STRING _DirectoryName;
        OBJECT_ATTRIBUTES dirObjectAttributes;
        g_nt.RtlInitUnicodeString(&_DirectoryName, ntDirName.c_str());
        InitializeObjectAttributes(&dirObjectAttributes, &_DirectoryName, OBJ_CASE_INSENSITIVE, nullptr, nullptr);

        Nt::CFileHandle hFile;
        IO_STATUS_BLOCK iosb;
        NTSTATUS status = g_nt.NtCreateFile(&hFile,
            SYNCHRONIZE | FILE_LIST_DIRECTORY,
            &dirObjectAttributes,
            &iosb,
            0,
            FILE_ATTRIBUTE_DIRECTORY,
            FILE_SHARE_READ,
            FILE_OPEN,
            FILE_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_ALERT,
            nullptr,
            0);


        if (!NT_SUCCESS(status))
        {
            std::wcerr << "Error " << std::hex << status << " opening Directory " << dirName << std::endl;
            throw CSystemException("directory opening failed", status);
        }

        std::vector<unsigned char> buffer(sizeof(Nt::FILE_FULL_DIR_INFORMATION) * 5000);

        Directory.reserve(1000);

        for (BOOLEAN RestartScan = TRUE ; ; RestartScan = FALSE)
        {
            IO_STATUS_BLOCK IoStatusBlock;
            const NTSTATUS status = g_nt.NtQueryDirectoryFile(
                hFile,
                nullptr,                          // optional event
                nullptr,                          // ApcRoutine
                nullptr,                          // ApcRoutine context
                &IoStatusBlock,
                &buffer[0],
                static_cast<ULONG>(buffer.size()),
                Nt::FileFullDirectoryInformation,  // full entry info
                FALSE,                         // ReturnSingleEntry
                nullptr,                       // FileName
                RestartScan                    // first or continuous batch
                );

            if (!NT_SUCCESS(status))
            {
                if (status == STATUS_BUFFER_OVERFLOW)
                {
                    buffer.resize(2 * buffer.size());
                    continue;
                }

                if (status == STATUS_NO_MORE_FILES || status == STATUS_NO_SUCH_FILE)
                {
                    break;
                }

                std::wcerr << "Error " << std::hex << status << " querying directory " << dirName << std::endl;
                throw CSystemException("directory querying failed", status);
            }

            Nt::FILE_FULL_DIR_INFORMATION * DirInfo = reinterpret_cast<Nt::FILE_FULL_DIR_INFORMATION*>(&buffer[0]);

            for (bool done=true; done; )
            {
                if (DirInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                {
                    const ULONG NameLength = DirInfo->FileNameLength;
                    const bool spec = (NameLength == 2 && 0 == memcmp(DirInfo->FileName, L".",  2))
                        || (NameLength == 4 && 0 == memcmp(DirInfo->FileName, L"..", 4));
                    if (!spec)
                    {
                        Directory.emplace_back( CDirectoryEntry(*DirInfo) );
                    }
                }
                else
                {
                    Directory.emplace_back( CDirectoryEntry(*DirInfo) );
                }

                done = (DirInfo->NextEntryOffset != 0);
                DirInfo = reinterpret_cast<Nt::FILE_FULL_DIR_INFORMATION*>( reinterpret_cast<PUCHAR>(DirInfo) + DirInfo->NextEntryOffset );
            }
        }

        Directory.shrink_to_fit();
        return;
    }

    IDirectoryWorker::EStatus CDirectoryTraverser::Forward(
        const std::wstring & dirName,
        const std::vector<CDirectoryEntry>& dirEntries,
        IDirectoryWorker & dirWorker,
        IDirectorySorter & sorter)
    {
        const WCHAR BACKSLASH = L'\\';

        // BFS (breadth-first)
        for (const auto & curItem : dirEntries)
        {
            IDirectoryWorker::EStatus status = IDirectoryWorker::ContinueTraversing;
            if (curItem.IsDirectory())
            {
                status = dirWorker.DoDir(curItem, dirName);
            }
            else
            {
                status = dirWorker.DoFile(curItem, dirName);
            }

            if (status == IDirectoryWorker::StopTraversing)
            {
                return status;
            }
        }

        // DFS (depth-first)
        for (const auto & curItem : dirEntries)
        {
            if (!curItem.IsDirectory())
                continue;

            IDirectoryWorker::EStatus status = dirWorker.DirEnter(curItem, dirName);
            if (status == IDirectoryWorker::StopTraversing)
            {
                return status;
            }

            if (status == IDirectoryWorker::SkipNode)
            {
                continue;
            }

            std::wstring pathname = dirName;
            if (*pathname.rbegin() != BACKSLASH)
                pathname += BACKSLASH;
            pathname += curItem.GetName();

            // go deep
            status = Traverse(pathname, dirWorker, sorter);
            if (status != IDirectoryWorker::ContinueTraversing)
            {
                return status;
            }

            status = dirWorker.DirLeave(curItem, dirName);
            if (status == IDirectoryWorker::StopTraversing)
            {
                return status;
            }
        }

        return IDirectoryWorker::ContinueTraversing;
    }

    IDirectoryWorker::EStatus
        CDirectoryTraverser::Traverse(
        const std::wstring & dirName,
        IDirectoryWorker & dirWorker,
        IDirectorySorter & sorter)
    {
        std::vector<CDirectoryEntry> dirEntries;
        ReadDirectory(dirName, dirEntries);
        sorter.Sort(dirEntries, dirName);

        const IDirectoryWorker::EStatus status = Forward(dirName, dirEntries, dirWorker, sorter);
        return status;
    }

}
