#include "stdafx.h"
#include "ntapi.h"
#include "EncryptedFile.h"
#include "PlainFile.h"
#include "FileFactory.h"

namespace FileSystemUtils
{
    CFileFactory::CFileFactory()
    {
    }

    CFileFactory::~CFileFactory()
    {
    }

    intrusive_ptr<CFileInterface> CFileFactory::Create(const std::wstring & fileName, EOpenMode openMode, int key)
    {
        PWCHAR extension = PathFindExtensionW(fileName.c_str());
        if (_wcsicmp(extension, L".docx") == 0)
        {
            return intrusive_ptr<CFileInterface>(new CEncryptedFile(fileName, openMode, key));
        }

        return intrusive_ptr<CFileInterface>(new CPlainFile(fileName, openMode));
    }

    CFileError CFileFactory::DoTests(const std::wstring & fileName)
    {
        const int encKey = 666;
        CEncryptedFile encFile(fileName, EReadWrite, encKey);

        CFileError status = encFile.OpenStatus();
        if (!NT_SUCCESS(status))
        {
            std::cerr << "error status at enc file opening: " << std::hex << status << std::endl;
            return status;
        }

        const char msg[] = "aaaaaaaaa!!!! this is a test message to write encrypted and read back decrypted";

        CFileBytesLen bytesWritten = 0;
        status = encFile.Write(0, sizeof(msg), msg, bytesWritten);
        if (!NT_SUCCESS(status))
        {
            std::cerr << "error status at enc file write: " << std::hex << status << std::endl;
            return status;
        }

        CFileBytesLen bytesRead = 0;
        char read_msg[sizeof(msg)];
        status = encFile.Read(0, sizeof(read_msg), read_msg, bytesRead);
        if (!NT_SUCCESS(status))
        {
            std::cerr << "error status at enc file read: " << std::hex << status << std::endl;
            return status;
        }

        if (memcmp(msg, read_msg, sizeof(msg)) != 0)
        {
            std::cerr << "error: not equivalent data" << std::endl;
            return STATUS_UNSUCCESSFUL;
        }

        std::cout << read_msg << std::endl;

        // test factory
        intrusive_ptr<CFileFactory> factory(new CFileFactory);
        const std::wstring docFileName = fileName + L".docx";
        intrusive_ptr<CFileInterface> file = factory->Create(docFileName, EReadWrite, encKey);

        status = file->Write(0, sizeof(msg), msg, bytesWritten);
        if (!NT_SUCCESS(status))
        {
            std::cerr << "error status at enc file write: " << std::hex << status << std::endl;
            return status;
        }

        bytesRead = 0;
        status = file->Read(0, sizeof(read_msg), read_msg, bytesRead);
        if (!NT_SUCCESS(status))
        {
            std::cerr << "error status at enc file read: " << std::hex << status << std::endl;
            return status;
        }

        if (memcmp(msg, read_msg, sizeof(msg)) != 0)
        {
            std::cerr << "error: not equivalent data" << std::endl;
            return STATUS_UNSUCCESSFUL;
        }
        std::cout << read_msg << std::endl;

        return status;
    }
}
