#pragma once

#include "DirectoryEntry.h"
#include "DirectorySorter.h"

namespace FileSystemUtils
{
    class CDirectoryIteratorLevel
    {
        friend class CDirectoryIterator;
        public:
            void Next() { ++m_Pos; }

            CDirectoryIteratorLevel()
            {
            }

            CDirectoryIteratorLevel(CDirectoryIteratorLevel&& r)
            {
                m_Path.swap(r.m_Path);
                m_Dir.swap(r.m_Dir);
                m_Pos = r.m_Pos;
            }

            CDirectoryIteratorLevel& operator=(CDirectoryIteratorLevel&& r)
            {
                m_Path.swap(r.m_Path);
                m_Dir.swap(r.m_Dir);
                m_Pos = r.m_Pos;
                return *this;
            }

        private:
            std::vector<CDirectoryEntry>   m_Dir;
            size_t                         m_Pos;
            std::wstring                   m_Path;

    };

    class CDirectoryIterator
    {
        public:
            CDirectoryIterator(const std::wstring& p, IDirectorySorter & sorter);
            ~CDirectoryIterator();

            void operator ++ ();
            const std::wstring & GetPath() const;

            CDirectoryEntry * operator -> ();
            bool IsValid() const;
            operator bool () const;

        private:
            CDirectoryIterator(const CDirectoryIterator&) = delete;
            CDirectoryIterator& operator=(const CDirectoryIterator&) = delete;

            void PushLevel(const std::wstring & dirName);
            void SkipEmptyLevels();

        private:
            std::vector<CDirectoryIteratorLevel>   m_Stack;
            IDirectorySorter &                     m_Sorter;
    };
}

