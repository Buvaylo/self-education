#include "stdafx.h"
#include "DirectoryEntry.h"

namespace FileSystemUtils
{

    CDirectoryEntry::CDirectoryEntry(const Nt::FILE_FULL_DIR_INFORMATION & dirInfo)
        : m_CreationTime(dirInfo.CreationTime.QuadPart)
        , m_LastAccessTime(dirInfo.LastAccessTime.QuadPart)
        , m_LastWriteTime(dirInfo.LastWriteTime.QuadPart)
        , m_ChangeTime(dirInfo.ChangeTime.QuadPart)
        , m_EndOfFile(dirInfo.EndOfFile.QuadPart)
        , m_AllocationSize(dirInfo.AllocationSize.QuadPart)
        , m_FileAttributes(dirInfo.FileAttributes)
        , m_FileName(dirInfo.FileName, dirInfo.FileNameLength/sizeof(WCHAR))
    {
    }

    bool CDirectoryEntry::IsDirectory() const
    {
        return (m_FileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
    }

    const std::wstring & CDirectoryEntry::GetName() const
    {
        return m_FileName;
    }

    ULONGLONG CDirectoryEntry::GetFileSize() const
    {
        return m_EndOfFile;
    }

    ULONGLONG CDirectoryEntry::GetAllocationSize() const
    {
        return m_AllocationSize;
    }

}

