#pragma once

#include "DirectorySorter.h"

namespace FileSystemUtils
{
    class CDirectorySorterRandom : public IDirectorySorter
    {
    public:
        void Sort(std::vector<CDirectoryEntry> & dirEntries, const std::wstring & parent) override;
        void Sort(std::vector<CDirectoryEntryPtr> & dirEntries, const std::wstring & parent) override;
    };
}
