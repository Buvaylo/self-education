#pragma once

#include "DirectoryWorker.h"

namespace FileSystemUtils
{

    class CDirectoryWorkerCounter : public IDirectoryWorker
    {
    public:
        ~CDirectoryWorkerCounter();
        CDirectoryWorkerCounter();

        void Print(std::ostream & out);

        IDirectoryWorker::EStatus DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;
        IDirectoryWorker::EStatus DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent) override;

    private:
        struct CDirectorySummary
        {
            CDirectorySummary()
                : m_FileCount(0)
                , m_DirCount(0)
                , m_SummaryFileSize(0)
                , m_SummaryAllocationSize(0) {}

            unsigned long         m_FileCount;
            unsigned long         m_DirCount;
            unsigned long long    m_SummaryFileSize;
            unsigned long long    m_SummaryAllocationSize;
        };

    private:
        std::vector<CDirectorySummary> m_DirectoryStack;
    };

}
