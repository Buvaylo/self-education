#include "stdafx.h"
#include "DirectorySorterOrdered.h"

namespace FileSystemUtils
{

    CDirectorySorterOrdered::CDirectorySorterOrdered(bool SortContent, bool SortAscendant, bool GroupDirectories, bool DirectoriesFirst)
        : m_SortContent(SortContent)
        , m_SortAscendant(SortAscendant)
        , m_GroupDirectories(GroupDirectories)
        , m_DirectoriesFirst(DirectoriesFirst)
    {
    }

    CDirectorySorterOrdered::~CDirectorySorterOrdered()
    {
    }

    void CDirectorySorterOrdered::Sort(std::vector<CDirectoryEntry> & dirEntries, const std::wstring & parent)
    {
        // implement here strategy for preprocessing directory content list: sorted asc/desc folders first/last etc
        if (m_SortContent)
        {
            std::sort(dirEntries.begin(), dirEntries.end(), *this );
        }
    }

    void CDirectorySorterOrdered::Sort(std::vector<CDirectoryEntryPtr> & dirEntries, const std::wstring & parent)
    {
        // implement here strategy for preprocessing directory content list: sorted asc/desc folders first/last etc
        if (m_SortContent)
        {
            std::sort(dirEntries.begin(), dirEntries.end(), *this );
        }
    }

    bool CDirectorySorterOrdered::operator() (const CDirectoryEntry & a, const CDirectoryEntry & b) const
    {
        if (m_GroupDirectories)
        {
            if (a.IsDirectory() && !b.IsDirectory())
            {
                return m_DirectoriesFirst;
            }
            if (!a.IsDirectory() && b.IsDirectory())
            {
                return !m_DirectoriesFirst;
            }
        }

        // both are file or dirs
        if (m_SortAscendant)
        {
            return _wcsicmp(a.GetName().c_str(), b.GetName().c_str()) < 0;
        }
        else
        {
            return _wcsicmp(a.GetName().c_str(), b.GetName().c_str()) > 0;
        }
    }

    bool CDirectorySorterOrdered::operator() (const CDirectoryEntryPtr & a, const CDirectoryEntryPtr & b) const
    {
        if (m_GroupDirectories)
        {
            if (a->IsDirectory() && !b->IsDirectory())
            {
                return m_DirectoriesFirst;
            }
            if (!a->IsDirectory() && b->IsDirectory())
            {
                return !m_DirectoriesFirst;
            }
        }

        // both are file or dirs
        if (m_SortAscendant)
        {
            return _wcsicmp(a->GetName().c_str(), b->GetName().c_str()) < 0;
        }
        else
        {
            return _wcsicmp(a->GetName().c_str(), b->GetName().c_str()) > 0;
        }
    }

}
