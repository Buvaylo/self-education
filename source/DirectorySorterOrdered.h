#pragma once

#include "DirectorySorter.h"

namespace FileSystemUtils
{
    class CDirectorySorterOrdered : public IDirectorySorter
    {
    public:
        CDirectorySorterOrdered(bool SortContent, bool SortAscendant, bool GroupDirectories, bool DirectoriesFirst);
        ~CDirectorySorterOrdered();
        void Sort(std::vector<CDirectoryEntry> & dirEntries, const std::wstring & parent) override;
        void Sort(std::vector<CDirectoryEntryPtr> & dirEntries, const std::wstring & parent) override;

        bool operator() (const CDirectoryEntry & a, const CDirectoryEntry & b) const;
        bool operator() (const CDirectoryEntryPtr & a, const CDirectoryEntryPtr & b) const;

    private:
        bool  m_SortContent;
        bool  m_SortAscendant;
        bool  m_GroupDirectories;
        bool  m_DirectoriesFirst;
    };
}
