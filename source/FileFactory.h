#pragma once

#include "FileInterface.h"
#include "intrusive_ptr.h"

namespace FileSystemUtils
{
    class CFileFactory : public CRefCountedBase
    {
        public:
           CFileFactory();
           virtual ~CFileFactory();
           virtual intrusive_ptr<CFileInterface> Create(const std::wstring & fileName, EOpenMode openMode, int key);
           static CFileError DoTests(const std::wstring & fileName);

    };
}
