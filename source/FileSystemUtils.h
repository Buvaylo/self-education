#pragma once

namespace FileSystemUtils
{
    typedef LONG64 CFilePos;

    void CopyFile(const std::wstring & srcFileName, const std::wstring & dstFileName);
    void CompareFiles(const std::wstring & srcFileName, const std::wstring & dstFileName);
    void TestCopyFile(const std::wstring & srcFileName, const std::wstring & dstFileName, CFilePos fileSize, int numThreads);
    void CreateRandomFile(const std::wstring & srcFileName, CFilePos fileSize);
}
