#include "stdafx.h"
#include "ntapi.h"
#include "SystemException.h"
#include "DirectoryTraverser.h"
#include "DirectoryIterator.h"
#include "DirectoryVisitorInterface.h"
#include "EncryptedFile.h"
#include "FileFactory.h"
#include "ThreadPool.h"
#include "FileSystemUtils.h"
#include "DirectorySorterOrdered.h"
#include "DirectoryWorkerPrinter.h"
#include "DirectoryWorkerCounter.h"
#include "DirectoryWorkerWebPrinter.h"

#define DirScanObserver           L"--observer"
#define DirScanIterator           L"--iterator"
#define DirVisitorIterator        L"--visitor"
#define DirWebCommand             L"--web"
#define TestCopyCommand           L"--testcopy"
#define CopyCommand               L"--copyfile"
#define EncryptFileCommand        L"--encryption"


namespace {

    int PrintUsage(const WCHAR progName[])
    {
        wprintf(L"Usage:\n");
        wprintf(L"%ws " DirScanIterator L" dirname\n", progName);
        wprintf(L"%ws " DirVisitorIterator L" dirname\n", progName);
        wprintf(L"%ws " DirScanObserver L" dirname\n", progName);
        wprintf(L"%ws " TestCopyCommand L" srcfile dstfile numthreads filesize(MB)\n", progName);
        wprintf(L"%ws " CopyCommand L" srcfile dstfile numthreads\n", progName);
        wprintf(L"%ws " EncryptFileCommand L" filename\n", progName);
        wprintf(L"%ws " DirWebCommand L" dirname\n", progName);

        return 1;
    }

    void RunDirectoryIterator(const std::wstring & dirName)
    {
        const bool SortContent = true;
        const bool SortAscendant = true;
        const bool GroupDirectories = true;
        const bool DirectoriesFirst = true;
        FileSystemUtils::CDirectorySorterOrdered sorter(SortContent, SortAscendant, GroupDirectories, DirectoriesFirst);

        for (FileSystemUtils::CDirectoryIterator it(dirName, sorter); it; ++it)
        {
            const std::wstring & fileName = it->GetName();
            const std::wstring & pathName = it.GetPath();
            wprintf(L"%ws\\%ws\n", pathName.c_str(), fileName.c_str());
        }
    }

    void RunDirectoryObserver(const std::wstring & dirName)
    {
        FileSystemUtils::CDirectoryTraverser DirectoryTraverser;

        // 1 scan: just print content
        const bool SortContent = true;
        const bool SortAscendant = true;
        const bool GroupDirectories = true;
        const bool DirectoriesFirst = true;
        FileSystemUtils::CDirectorySorterOrdered sorter(SortContent, SortAscendant, GroupDirectories, DirectoriesFirst);

        FileSystemUtils::CDirectoryWorkerPrinter dirPrinter;
        double estTime = Nt::GetSecs();
        DirectoryTraverser.Traverse(dirName, dirPrinter, sorter);
        estTime = Nt::GetSecs() - estTime;
        std::cout << "1 scan time required = " << std::fixed << std::setprecision(3) << estTime << std::endl;

        // 2 scan: compute statistic (sizes, number of files, etc)
        FileSystemUtils::CDirectoryWorkerCounter dirCounter;
        estTime = Nt::GetSecs();
        DirectoryTraverser.Traverse(dirName, dirCounter, sorter);
        estTime = Nt::GetSecs() - estTime;
        std::cout << "2 scan time required = " << std::fixed << std::setprecision(3) << estTime << std::endl;
        dirCounter.Print(std::cout);
    }

    int TestAllSafe(int argc, WCHAR *argv[])
    {
        int error = 0;
        if (argc == 3 && wcscmp(argv[1], DirScanObserver) == 0)
        {
            RunDirectoryObserver(argv[2]);
        }
        else if (argc == 3 && wcscmp(argv[1], DirScanIterator) == 0)
        {
            RunDirectoryIterator(argv[2]);
        }
        else if (argc == 3 && wcscmp(argv[1], DirVisitorIterator) == 0)
        {
            FileSystemUtils::RunDirectoryVisitorIterator(argv[2]);
        }
        else if (argc == 6 && wcscmp(argv[1], TestCopyCommand) == 0)
        {
            // file copy tests
            std::wstring srcFileName(argv[2]);
            std::wstring dstFileName(argv[3]);

            const int numThreads = max(_wtoi(argv[4]), 1);

            FileSystemUtils::CFilePos fileSize = max(_wtoi(argv[5]), 1);
            fileSize *= 1024 * 1024;
            FileSystemUtils::TestCopyFile(srcFileName, dstFileName, fileSize, numThreads);
        }
        else if (argc == 5 && wcscmp(argv[1], CopyCommand) == 0)
        {
            // file copying
            std::wstring srcFileName(argv[2]);
            std::wstring dstFileName(argv[3]);

            int numThreads = max(_wtoi(argv[4]), 1);

            if (!utils::CThreadPool::GetInstance().InitializeThreadPool(numThreads))
            {
                printf("error!\n");
            }

            FileSystemUtils::CopyFile(srcFileName, dstFileName);
            FileSystemUtils::CompareFiles(srcFileName, dstFileName);
        }
        else if (argc==3 && wcscmp(argv[1], EncryptFileCommand) == 0)
        {
            std::wstring fileName(argv[2]);
            FileSystemUtils::CFileFactory::DoTests(fileName);
        }
        else if(argc == 3 && wcscmp(argv[1], DirWebCommand) == 0)
        {
            error = FileSystemUtils::RunWebServer(argv[2]);
        }
        else
        {
            error = PrintUsage(argv[0]);
        }

        return error;
    }

}

int wmain(int argc, WCHAR *argv[])
{
    std::ios_base::sync_with_stdio(false);
    std::wcin.tie(nullptr);

    int error = 0;
    try
    {
        error = TestAllSafe(argc, argv);
    }
    catch (CSystemException & ex)
    {
        std::cerr << ex.what() << std::endl
            << "error=" << std::hex << ex.GetErrorCode() << std::endl;
        error = 1;
    }
    catch (std::exception & ex)
    {
        std::cerr << ex.what() << std::endl;
        error = 1;
    }
    catch(...)
    {
        std::cerr << "unknown exception" << std::endl;
        error = 1;
    }

    return error;
}
