// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#pragma warning( disable : 4100 ) // unreferenced formal parameter

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <ws2def.h>
#include <winsock2.h>
#include <io.h>
#include <fcntl.h>
#include <string.h>
#include <wchar.h>
#include <shlwapi.h>
//#include <ntsecapi.h>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <iterator>

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <malloc.h>

#include <atlbase.h>

#include <sys\timeb.h>
#include <io.h>    
#include <ctime>
