#pragma once

#include "DirectoryEntry.h"
#include "DirectoryEntryInterface.h"

namespace FileSystemUtils
{
    // abstract interface for directory content sorting strategy
    class IDirectorySorter
    {
    public:
        virtual ~IDirectorySorter() {}
        // This interface is not clearly abstract and provides default behaviour - any sorting
        virtual void Sort(std::vector<CDirectoryEntry> & dirEntries, const std::wstring & parent) {}
        virtual void Sort(std::vector<CDirectoryEntryPtr> & dirEntries, const std::wstring & parent) {}
    };

}
