#pragma once

#include "intrusive_ptr.h"

namespace FileSystemUtils
{
    enum EOpenMode {
        ERead      = 1,
        EWrite     = 2,
        EReadWrite = ERead | EWrite
    };

    typedef long CFileError;
    typedef ULONG CFileBytesLen;

    class CFileInterface : public CRefCountedBase
    {
    public:
        virtual ~CFileInterface() {}
        virtual CFileError OpenStatus() const = 0;
        virtual CFileError Open(const std::wstring& fileName, EOpenMode openMode) = 0;
        virtual CFileError Read(LONG64 offset, CFileBytesLen nbytes, void *buffer, CFileBytesLen & bytes) = 0;
        virtual CFileError Write(LONG64 offset, CFileBytesLen nbytes, const void *buffer, CFileBytesLen & bytes) = 0;
        virtual CFileError Close() = 0;
    };

}
