#pragma once

#include "DirectoryEntry.h"

namespace FileSystemUtils
{
    // abstract interface for directory structure observer/handler
    class IDirectoryWorker
    {
    public:
        typedef enum
        {
            ContinueTraversing,
            SkipNode,
            StopTraversing,
            Error
        } EStatus;

        virtual ~IDirectoryWorker() {};
        virtual EStatus DirEnter(const CDirectoryEntry& dirEntry, const std::wstring & parent) = 0;
        virtual EStatus DirLeave(const CDirectoryEntry& dirEntry, const std::wstring & parent) = 0;
        virtual EStatus DoFile(const CDirectoryEntry& dirEntry, const std::wstring & parent) = 0;
        virtual EStatus DoDir(const CDirectoryEntry& dirEntry, const std::wstring & parent) = 0;
    };

}

