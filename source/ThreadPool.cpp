#include "stdafx.h"
#include "ThreadPool.h"

namespace utils
{

    CThreadPool CThreadPool::s_ThreadPoolInstance;

    CThreadPool & CThreadPool::GetInstance()
    {
        return s_ThreadPoolInstance;
    }

    enum {
        ThreadStop = 1
    };

    CThreadPool::CThreadPool()
        : m_IoCompletionPortHandle(nullptr)
        , m_ExitFlag(false)
        , m_TaskCounter(0)
    {
    }

    CThreadPool::~CThreadPool()
    {
        StopThreadPool();
    }

    int CThreadPool::GetThreadNum() const
    {
        return static_cast<int>(m_WorkingThreads.size());
    }

    bool CThreadPool::SendCommand(CThreadPoolTaskInterface* pTask, LPVOID param)
    {
        const BOOL result =
            PostQueuedCompletionStatus(
            m_IoCompletionPortHandle,
            0,
            (ULONG_PTR)pTask,
            reinterpret_cast<LPOVERLAPPED>(param)
            );

        if (result)
        {
            InterlockedIncrement(&m_TaskCounter);
            return true;
        }

        return false;
    }

    bool CThreadPool::SendCommand(ULONG_PTR code)
    {
        return SendCommand(this, reinterpret_cast<LPVOID>(code));
    }

    void CThreadPool::AddThread()
    {
        DWORD dwThreadID = 0;
        HANDLE hThread = CreateThread(0, 0, WorkerThreadProcedure, this, 0, &dwThreadID);
        if (hThread != nullptr)
            m_WorkingThreads.emplace_back(hThread);
    }

    bool CThreadPool::InitializeThreadPool(int maxThreadNum)
    {
        StopThreadPool();

        m_ExitFlag = false;
        m_WorkingThreads.clear();

        maxThreadNum = min(maxThreadNum, 128);

        m_IoCompletionPortHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, nullptr, 0, 0);
        if(m_IoCompletionPortHandle == nullptr)
        {
            return false;
        }

        for (int i=0; i <maxThreadNum; ++i)
        {
            AddThread();
        }

        return true;
    }

    bool CThreadPool::StopThreadPool()
    {
        if (m_IoCompletionPortHandle == nullptr)
        {
            return true;
        }

        m_ExitFlag = true;
        const size_t nThreadCount = m_WorkingThreads.size();

        for (size_t i=0; i<nThreadCount; ++i)
        {
            SendCommand(ThreadStop);
            // ignore result, do nothing on failure
        }

        for (size_t i=0; i<nThreadCount; ++i)
        {
            HANDLE hThread = m_WorkingThreads[i];
            WaitForSingleObject(hThread, INFINITE);
            CloseHandle(hThread);
        }
        m_WorkingThreads.clear();

        CloseHandle(m_IoCompletionPortHandle);
        m_IoCompletionPortHandle = nullptr;

        return true;
    }

    bool CThreadPool::EnQueue(CThreadPoolTaskInterface* pTask, LPVOID param)
    {
        if (m_ExitFlag || nullptr == m_IoCompletionPortHandle)
        {
            return false;
        }

        const bool result = SendCommand(pTask, param);
        return result;
    }

    DWORD WINAPI CThreadPool::WorkerThreadProcedure(LPVOID lpThreadParam)
    {
        CThreadPool* pThreadPoolObj = reinterpret_cast<CThreadPool*>(lpThreadParam);

        for (;;)
        {
            DWORD dwNumberOfBytes = 0;
            ULONG_PTR pTaskParam = 0;
            LPOVERLAPPED pJobParam = 0;

            if( FALSE == GetQueuedCompletionStatus(
                pThreadPoolObj->m_IoCompletionPortHandle,
                &dwNumberOfBytes,
                &pTaskParam,
                &pJobParam,
                INFINITE ))
            {
                const DWORD dwError = GetLastError();
                std::cerr << __FUNCTION__ ": get queue failure " << std::hex << dwError << std::endl;
                break;
            }

            InterlockedDecrement(&pThreadPoolObj->m_TaskCounter);

            CThreadPoolTaskInterface* pTask = reinterpret_cast<CThreadPoolTaskInterface*>(pTaskParam);

            // ThreadPool send itself as task interface
            if (pTask == pThreadPoolObj)
            {
                const ULONG_PTR code = reinterpret_cast<ULONG_PTR>(pJobParam);
                switch (code)
                {
                    // signal for cancellation
                case ThreadStop:
                    return 0;

                default:
                    std::cerr << __FUNCTION__ ": unknown thread pool code " << code << std::endl;
                    break;
                }
            }

            if (!pThreadPoolObj->m_ExitFlag)
            {
                LPVOID pJobInput = reinterpret_cast<LPVOID>(pJobParam);
                try
                {
                    pTask->DoWork(pJobInput);
                }
                catch(const std::exception & ex)
                {
                    std::cerr << ex.what() << std::endl;
                }
                catch(...)
                {
                    std::cerr << "unknown exception" << std::endl;
                }
            }
        }

        return 0;
    }

}
