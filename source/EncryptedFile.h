#pragma once

#include "FileInterface.h"
#include "PlainFile.h"

namespace FileSystemUtils
{

    class CEncryptedFile : public CFileInterface
    {
    public:
        ~CEncryptedFile();
        CEncryptedFile(const std::wstring& fileName, EOpenMode openMode, int key);
        CFileError OpenStatus() const override;
        CFileError Open(const std::wstring& fileName, EOpenMode openMode) override;
        CFileError Read(LONG64 offset, CFileBytesLen nbytes, void *buffer, CFileBytesLen & bytes) override;
        CFileError Write(LONG64 offset, CFileBytesLen nbytes, const void * buffer, CFileBytesLen & bytes) override;
        CFileError Close() override;

    private:
        unsigned char m_enc[256];
        unsigned char m_dec[256];
        CPlainFile m_File;
    };

}
