rem run all tests from selfeducation.exe
selfeducation.exe --iterator e:\bdp
selfeducation.exe --observer e:\bdp
selfeducation.exe --observer e:\virtual_share
selfeducation.exe --visitor e:\bdp
selfeducation.exe --visitor e:\virtual_share
selfeducation.exe --encryption e:\bdp\selfeducation\X64\Release\test_enc.tmp
selfeducation.exe --testcopy src.tmp dst.tmp 8 100
selfeducation.exe --copyfile src.tmp dst.tmp 4
